close all; clear; clc;

%% settings
iterations = 1e6;
l1 = 3; % input feature size
l2 = 10; % output feature size
matrix_type = "dense"; % "dense" or "convolutional"

if strcmp(matrix_type, "convolutional")
  % assume stride=1, we define kernel size such that output feature size is l2
  kernel_size = l2-l1+1; 
elseif strcmp(matrix_type, "dense")
  kernel_size = 0; % doesn't matter for fully-connected network, only needed to avoid error
end
epsilon = 0.05; % epsilon in Lemma 5

%% arbitray x and y
x = [1 0.5 1.5]';
y = [-0.5 3 -1]';
% x = randn(l1, 1);
% y = randn(l1, 1);

%% Compute Q_xy
sum = zeros(l1);
parfor i = 1:iterations
  AxAy = compute_AxAy(l2, l1, x, y, matrix_type, kernel_size);
  sum = sum + AxAy;
end
Qxy = sum / iterations

%% Evaluate probability P(|Ax'*Ay - Q_xy| < epsilon)
count_near = 0;
parfor i = 1:iterations
  AxAy = compute_AxAy(l2, l1, x, y, matrix_type, kernel_size);
  diff = AxAy - Qxy;
  count_near = count_near + (spectral_norm(diff) < epsilon);
end
prob = count_near / iterations;
prob


function l2 = spectral_norm(A)
  l2 = max(sqrt(eig(A.'*A)));
end

function AxAy = compute_AxAy(l2, l1, x, y, matrix_type, kernel_size)
  A = create_A(l2, l1, matrix_type, kernel_size);
  Ax = diag(A*x>0)*A;
  Ay = diag(A*y>0)*A;
  AxAy = Ax' * Ay;
end

function A = create_A(l2, l1, matrix_type, kernel_size)
  if strcmp(matrix_type, "dense")
    A = randn(l2, l1) / l2;
  elseif strcmp(matrix_type, "convolutional")
    A = zeros(l2, l1);
    kernel = randn(kernel_size, 1) / l2; % what should be the normalization factor here?
    for i = 1 : l1
      A(i:i+kernel_size-1, i) = kernel;
    end
  else
    error("unknown type")
  end
end