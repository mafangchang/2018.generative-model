close all; clear; clc;

% Ground Truth
k = 2;

% X is a SINGLE array of dimension H x W x D x N where 
%   (H,W) are the height and width of the image stack, 
%   D is the image depth (number of feature channels), 
%   N the number of of images in the stack.
z_gt = single(3 * randn(1,1,k)); % dont forget 'single'

weight_type = '{-1,1}'; % 'gaussian', '{0,1}', '{-1,1}', '[-1,1]', '1'
measurement_type = 'subsample'; % 'subsample', 'gaussian'
activation = 'relu'; % 'relu', 'leakyrelu', 'sigmoid', 'tanh'
doplot = true;


%% Define network 
net.layers = {} ;
kernel_size = 3;
stride = 2;
filters = [2, 4, 8, 1];
num_out_channels = filters(end);
% F is a SINGLE array of dimension FW x FH x K x FD where 
%   (FH,FW) are the filter height and width, 
%   K the number of filters in the bank, 
%   FD the depth of a filter (the same as the depth of image X).
for i = 2 : length(filters)
    net.layers{end+1} = struct(...
        'type', 'convt', ...
        'weights', {
            {create_weights(kernel_size,filters(i),filters(i-1),weight_type), ...
            zeros(1, 1, filters(i), 'single') ...
        }}, ...
        'upsample', stride);
    net.layers{end+1} = get_activation(activation);
end
net = vl_simplenn_tidy(net);


%% Generative model
G = @(z)forward_pass(z, net);
x_gt = G(z_gt);
x_gt = x_gt(:);

%% Sampling
num_out = numel(x_gt);
m = floor(num_out*0.5);
% m = num_out;

if strcmp(measurement_type, 'gaussian')
  % Gaussian Measurements
  A = create_weights(num_out, m, 'gaussian'); 
elseif strcmp(measurement_type, 'subsample')
  % Subsample Measurements
  A = eye(m, num_out); 
else
  error('invalid measurement_type')
end
noise = 0;
error = noise * randn(m,1);
%error = 0.1 * ones(m,1);
y = A * x_gt + error;

%% Loss function
C = @(z)(evaluate_loss(z, net, A, G, y));

%% Create random initial estimates
z0 = randn(1,1,k);     % Random initializer
%z0 = single(-z_gt/pi);

%% Optimization 
sOpt = optimset('fmin_adam');
sOpt.MaxFunEvals = 1e6;
sOpt.Display = 'iter';
sOpt.TolFun = 1e-9;
options = optimoptions('fminunc','Display', 'off','SpecifyObjectiveGradient',true);


t = cputime;
z_hat_1 = fminunc(C, z0, options);
z_hat_2 = fminunc(C, -z_hat_1, options);

% z_hat_1 = fmin_adam(C,z0,0.01, [], [], [], [], sOpt);  % Solve with Adam optimizer
% z_hat_2 = fmin_adam(C,-z_hat_1,0.01, [], [], [], [], sOpt); 

% z_hat_2 = z_hat_1;
t = cputime - t;

if C(z_hat_1)<C(z_hat_2)
  z_hat = z_hat_1;
else
  z_hat = z_hat_2;
end
x_hat = G(z_hat);

%% Plot cost function landscape
if doplot
  %% Evaluation
  diff_z = norm2(z_hat-z_gt);
  diff_x = norm2(x_hat(:)-x_gt);
  diff_C = C(z_hat)-C(z_gt);
  fprintf('|z_hat - z_gt|=%g \n', diff_z)
  fprintf('|x_hat - x_gt|=%g \n', diff_x)
  fprintf('|C_hat - C_gt|=%g \n', diff_C)
  fprintf('z_hat/z_gt=%.3f, z_gt/z_hat=%.3f \n', ...
    norm2(z_hat)/norm2(z_gt), norm2(z_gt)/norm2(z_hat))

  if (k==2) % && (diff_z > 1e-2)
    markersize = 40;
    fontsize = 20;
    gridsize = 4.5;
%     boundary = -1.0 : 0.01 : 1.0;
    boundary = -8.0 : 0.25 : 8;

    [XX,YY] = meshgrid(boundary,boundary);
    XX = XX + z_gt(1);
    YY = YY + z_gt(2);
    P = [XX(:),YY(:)];
    Cs = zeros(size(P,1), 1);
    for i = 1 : size(P,1)
      z_i = reshape(P(i,:)', [1, 1, k]);
      Cs(i) = C(z_i);
    end
    Cs = reshape(Cs, size(XX));
    
    fig1 = figure(1);
    set(gca,'position',[0 0 1 1],'units','normalized')
%     fig1.Renderer='Painters';
    cla;
    % surf(XX, YY, Cs);
    mesh(XX, YY, Cs);
    hold on
    contour(XX, YY, Cs, 'LevelStep', max(Cs(:))/100 )
    colormap('jet')

    hold on;
    z_offset=max(Cs(:))/100;
    p2=scatter3(z_gt(1), z_gt(2), C(z_gt)+z_offset, markersize+5, 'og', ...
      'MarkerFaceColor', 'g');
%     p3=scatter3(z_hat_1(1), z_hat_1(2), C(z_hat_1)+z_offset, markersize, 'hm', ...
%       'MarkerFaceColor', 'm');
%     p4=scatter3(z_hat_2(1), z_hat_2(2), C(z_hat_2)+z_offset, markersize, 'hk', ...
%       'MarkerFaceColor', 'k');
%     p5=scatter3(-z_gt(1)/pi, -z_gt(2)/pi, C(-z_gt/pi)+z_offset, 'hr', ...
%       'MarkerFaceColor', 'r');
    
    % contourf(XX, YY, Cs);
%     leg = legend([p2, p3, p4], 'ground truth', '$\hat{z_1}$', '$\hat{z_2}$')
%     set(leg,'Interpreter','latex');

    filename = sprintf('ac=%s-kernel=%d-stride=%d-m=%g-noise=%g.eps', ...
      activation, kernel_size, stride, m / num_out, noise);
%     print(filename,'-depsc')
  else
    disp('perfect reconstruction, so no plotting. program ends here.')
  end
end

function n = norm2(A) 
  n = norm(squeeze(A));
end

