# 2018.generative-model
iterative depth reconstruction using generative model and gradient descent

## python
python codes, including
 - training of a Generative Adversarial Network (GAN) model using PyTorch
 - training of a Variational AutoEncoder (VAE) model using PyTorch
 - image reconstruction using the trained models

#### requirements
- install [PyTorch](http://pytorch.org/)
- install the python scipy package with `pip install scipy`

## matlab
MatLab code for testing image reconstruction based on a generative model (neural network).

#### TODOs
[ ] play with [MatConvNet](http://www.vlfeat.org/matconvnet/)
