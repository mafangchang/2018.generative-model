\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2018
\PassOptionsToPackage{numbers, compress}{natbib}

% ready for submission
\usepackage[final]{neurips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add
% add the [preprint] option:
% \usepackage[preprint]{nips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
% \usepackage[final]{nips_2018}

% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography

% \input{preamble_packages}
\usepackage{graphicx}
\usepackage{xspace} % useful for adding \xspace for defining macros
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{prettyref}
\usepackage{color}
\usepackage{bm}

\input{../symbols/preamble_symbols}
\input{../symbols/shortcuts}

\usepackage{xr}
\externaldocument{../root}



\newcommand{\sm}{Sup.}
\renewcommand{\theequation}{\sm\arabic{equation}}
\renewcommand{\thefigure}{\sm\arabic{figure}}
\renewcommand{\theremark}{\sm\arabic{remark}}
\renewcommand{\thetable}{\sm\arabic{table}}
\renewcommand{\thesection}{\sm\arabic{section}}

% \def\thelemma{SM-\arabic{lemma}}  
\newtheorem{lem}{Lemma}[theorem]
\renewcommand\thelem{\sm\arabic{lem}}

\newtheorem{rem}{Remark}[theorem]
\renewcommand\therem{\sm\arabic{lem}}

% \newrefformat{fig}{SM-Fig.\,\ref{#1}}
%\newrefformat{lem}{SM-Fig.\,\ref{#1}}


% If accepted, instead use the following line for the camera-ready submission:
%\usepackage[accepted]{icml2018}

% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\title{Supplementary Materials \\Invertibility of Convolutional Generative \\Networks from Partial Measurements}

\author{
  Fangchang Ma*\\
  MIT\\
  \texttt{fcma@mit.edu} \\
  %% examples of more authors
  \And
  Ulas Ayaz\thanks{Both authors contributed equally to this work. Ulas Ayaz is presently affiliated with Lyft, Inc.}\\
  MIT\\
  \texttt{uayaz@mit.edu}\\
  % \texttt{ulasayaz@gmail.com}
  \texttt{uayaz@lyft.com}
  \And
  Sertac Karaman\\
  MIT\\
  \texttt{sertac@mit.edu}
}

\begin{document}
\maketitle

This document contains proof of the theorem and lemmas in the main document \cite{root}. Cross references to sections, figures, equations, theorems and lemmas in \cite{root} use the original numbering as they appear there; whereas references to this supplementary document start with \textit{SM-} prefix. 

\section{Notation}\label{sec:notation}


\begin{table}[htbp]
\centering
\caption{Summary of notation}
\begin{tabular}{| l | p{12cm} |}
\hline
Notation & Interpretation \\ \hline \hline
%%%%%%%%%%%%%%%%%%%%
$\bx, \bz$ & two arbitrary, non-zero vectors \\ \hline
$\bx^{(i)}, \bz^{(i)}$ & the corresponding output vectors at layer $i \in \{0, 1, 2\}$\\ \hline
$x_j^{(i)}$, $z_j^{(i)}$ & the $j^\text{th}$ block of $\bx^{(i)}, \bz^{(i)}$, no permutation \\ \hline
$\tilde{\bx}^{(i)}, \tilde{\bz}^{(i)}$ & the permuted vectors at layer $i \in \{0, 1, 2\}$\\ \hline
$\tx_j^{(i)}$, $\tz_j^{(i)}$ & the $j^\text{th}$ block of $\bx^{(i)}, \bz^{(i)}$ after permutation \\ \hline
$D_0, D_1, D_2$ & number of blocks in a vector (before $1^\text{st}$ layer, after permutation, and after $2^\text{nd}$ layer, respectively)\\ \hline
\hline
%%%%%%%%%%%%%%%%%%%%
$\bW_1, \bW_2$ & block weight matrices at each convolution layer \\ \hline
$\bW_{i, +, \bz}$ & effective weight matrices with ReLU taken into account, when input vector is $\bz$ \\ \hline
$\bQxz$ & expectation of $\bWx\tran \bWz$ \\ \hline
\hline
%%%%%%%%%%%%%%%%%%%%
$\btheta^{(i)}$ & the angle between two different vectors $\bx, \bz$ at layer $i$ \\ \hline
$\theta_j^{(i)}$ & the angle between the $j^\text{th}$ blocks of $x_j^{(i)}$, $z_j^{(i)}$ at layer $i$, without permutation \\ \hline
$\ttheta_j^{(i)}$ & the angle between the $j^\text{th}$ blocks of $\tx_j^{(i)}$, $\tz_j^{(i)}$ at layer $i$, with permutation \\ \hline
\hline
%%%%%%%%%%%%%%%%%%%%
$\tih_{\bx, \bz}$ & a random vector that $\bWonex\tran \bWtwox\tran \bWtwoz \bWonez \bz$ concentrates around\\ \hline
$h_{\bz, \bztrue}$ & the perturbation of $\tih_{\bz, \bztrue}$ around its mean $\bz/4$  \\ \hline
$S_{\epsilon,\bztrue}$ & a small region outside which the perturbation $h_{\bz, \bztrue}$ is very small \\ \hline
$v_{\bz,\bztrue}$ & descent direction at $\bz$ \\ \hline
%%%%%%%%%%%%%%%%%%%%
\end{tabular}
\label{tab:notation}
\end{table}

Before continuing to the proof of Theorem~\ref{thm:injective}, we introduce some notation. 

% Let ReLU operator $\si(z) = \max(z,0)$ apply entrywise. 
Let $\Id_n$ be an $n \times n$ identity matrix. If dimension is not specified, we assume it is clear from the context. Let $\diag(Az > 0)$ be a diagonal matrix, where $(i,i)^\text{th}=1$ if $(Az)_i > 0$, and $0$ otherwise. 
Let ${\cal B}(z,r)$ be an Euclidean ball of radius $r$ centered at $z$. Let $\Wonez = \diag(W_1 z > 0) W_1$ and $\Wtwoz = \diag(W_2 \Wonez z > 0) W_2$. For matrices, $\|A\|$ denotes the spectral norm. Let $S^{k-1}$ be the unit sphere in $\R^k$. 
A block vector $\bz = [z_i]_i^n \in \R^{kn}$ is a concatenation of $n$ vectors, each of size $k$, and uses boldface notation. Similarly, a diagonal block matrix is denoted $\bW = [W_i]_i^n$, with matrices $\{W_i\}$ on it diagonal. For any nonzero $z \in \R^k$, let $\hx = \frac{x}{\|x\|_2}$. For block vector $\bz = [z_i]_i^n $, let $\hbz = [\hz_i]_{i=1}^n$. For fixed $x,z \in \R^k$, let $\Mxz$ be the matrix such that $\Mxz \hx = \hz$, $\Mxz \hz = \hx$ and $\Mxz v = 0$ for all $v \in \mathrm{span}(\{x,z\})^\perp$. Then given block vectors $\bx = [x_i]_{i=1}^n,\bz = [z_i]_{i=1}^n$, 
let $\bMxz = [\Mxizi]_{i=1}^n$.  
Denote the block identity matrix $\bI = [\Id_k]_{i=1}^n$. Let $\angle(x,z)$ be the angle between two vectors $x$ and $z$. 
%$1_S = \left\{ \begin{array}{ll} 1 \ \ \text{if } S \\ 0 \ \ \text{otherwise} \end{array} \right.$. 


Recall that the weight matrix $\bW_i$ each for layers $i=1,2$ is assumed to be permuted to be a block matrix, as illustrated in \prettyref{fig:deconMatrix}(b). A corresponding permutation is also applied to the input vectors of each layers. 

Specifically, assume an input block vector $\bz = \bzo = [z_j^{(0)}]_{j=1}^{D_0}$, then the output of the first transposed convolution layer is $\bzone = \bWonez \bzo$ which also has $D_0$ channels (blocks). Before the second convolution layer, we apply another permutation such that the new vector $\tilde{\bz}^{(1)} = \mathrm{Perm}(\bzone)$ now has $D_1$ blocks. $\tilde{\bz}^{(1)}$ is then fed as an input to the second transpose convolution layer, resulting in an output $\bztwo = \bWtwoz \tilde{\bz}^{(1)}$. 

In addition, let $\btheta^{(i)} = \angle(\bx^{(i)},\bz^{(i)})$ denote the angle between two different vectors $\bx^{(i)},\bz^{(i)}$ at the $i^\text{th}$ layer. In particular, let $\theta_j^{(i)} = \angle(x_j^{(i)}, z_j^{(i)})$ denote the angle between the $j^\text{th}$ blocks of the two vectors. If the vectors are permuted, we use $\ttheta_j^{(i)} = \angle(\tx_j^{(i)}, \tz_j^{(i)})$. We also introduce the notation $\odot$ for  multiplication of a regular vector $a \in \R^n$ and a block vector $\bz = [z_j]_{j=1}^n$ in the following way: 
$$
a \odot \bz = [a_j z_j]_{j=1}^n.
$$
We use big-$\calO(\cdot)$ notation to denote the order of magnitude for a variable. Finally, we also use $\lesssim, \gtrsim$ and $\simeq$ when the inequalities and equalities are up to a small universal constant $\eps$ which may not be specified. For instance $x \simeq y$ indicates that $x = y + \calO(\eps)$. All vector and angle notations used are summarized in \prettyref{tab:notation}.

\input{proof}

%!TEX root = supplementary.tex

\subsection{Additional Lemmas}
Recall that the block weight matrix is defined as $\bW_1 = [W_1]_{j=1}^{D_0}$ where $W_1 \in \R^{C_1 \ell \times C_0}$. Similarly, $\bW_2 = [W_2]_{j=1}^{D_1}$ where $W_2 \in \R^{C_2 \ell \times C_1}$. In the following lemma, we extend the concentration of matrix products in \prettyref{lem:Central} to 2-layer networks.
\begin{lem}\label{lem:aux}
Fix $\eps \in (0,1)$. Let $W_1$ have i.i.d. ${\cal N}(0,1/{C_1 \ell})$ weights and $W_2$ have i.i.d. ${\cal N}(0,1/{C_2 \ell})$ weights. Assume the conditions of \prettyref{lem:Central} hold, then with high probability for all $\bz \not=\mathbf{0}$,
\begin{align}
\|\bWonez\tran \bWtwoz\tran \bWtwoz \bWonez - \bI /4\| \leq 2 \eps.
\end{align}
\end{lem}
\begin{proof}%[Proof for \prettyref{lem:aux}]
From \prettyref{lem:Central}, we know that for all $\bz \not=\mathbf{0}$, 
\begin{align*}
\|\bWonez\tran \bWonez - \bI /2\| & \leq \eps \\
\|\bWtwoz\tran \bWtwoz - \bI /2\| & \leq \eps
\end{align*}
Also, from \prettyref{eq:normbound2} we have
$$
\| \bWonez \|^2 \leq \frac{1}{2} + \eps.
$$
Using these two along with the triangle inequality, it follows that
\begin{align*}
& \quad \|\bWonez\tran \bWtwoz\tran \bWtwoz \bWonez - \bI /4\| \\
%%%%%
& = \bigg \| \left(\bWonez\tran \bWtwoz\tran \bWtwoz \bWonez - \frac 1 2 \bWonez\tran \bWonez \right) \\
& \quad\quad\quad + \left( \frac 1 2 \bWonez\tran \bWonez - \bI / 4 \right) \bigg\| \\
%%%%%
& \leq \|\bWonez\tran (\bWtwoz\tran \bWtwoz  - \bI / 2) \bWonez \| \\
& \quad\quad\quad + \frac 1 2 \|\bWonez\tran \bWonez - \bI / 2 \| \\
%%%%%
& \leq \|\bWonez\|^2 \|\bWtwoz\tran \bWtwoz  - \bI / 2 \| \\
& \quad\quad\quad + \frac 1 2 \|\bWonez\tran \bWonez - \bI / 2 \| \\
%%%%%t
& \leq \left(\frac{1}{2} + \eps \right) \epsilon + \frac 1 2 \epsilon \leq 2 \epsilon .
\end{align*}
\end{proof}

Given block vectors $\bx$ and $\bz$, we define the following vector
\begin{align}\label{eq:hxz}
&\tih_{\bx, \bz} := {\sst \left[\frac{\pi - \ttheta_j^{(1)}}{2\pi} \right]_{j=1}^{D_1} } \odot \left( {\sst \left[\frac{\pi - \theta_j^{(0)}}{2\pi} \right]_{j=1}^{D_0} } \odot \bz + {\sst \left[\frac{\sin \theta_j^{(0)} \|z_j^{(0)}\|_2}{2\pi \|x_j^{(0)}\|_2} \right]_{j=1}^{D_0} } \odot \bx  \right) + {\sst \left[\frac{\sin \ttheta_j^{(1)} \|\tz_j^{(1)}\|_2}{2\pi \|\tx_j^{(1)}\|_2} \right]_{j=1}^{D_1} } \odot \bx 
\end{align}
where $\ttheta_j^{(1)}, \tx_j^{(1)},\tz_j^{(1)}$ are defined as in \prettyref{sec:notation} and \prettyref{tab:notation}. Next we show that $\bWonex\tran \bWtwox\tran \bWtwoz \bWonez \bz$ concentrates around this random vector $\tih_{\bx, \bz}$.

\begin{lem}\label{lem:aux2}
Assume $\bW_1$ and $\bW_2$ satisfy the conditions of Lemma~\ref{lem:Central}. The for all $\bx \neq 0,\bz \neq 0$ we have
\begin{align*}
&\|\bWonex\tran \bWtwox\tran \bWtwoz \bWonez \bz - \tih_{\bx, \bz}\|_2 \lesssim \eps \max\{\|\bx\|_2,\|\bz\|_2\}. 
\end{align*}
\end{lem}

\begin{proof}
We expand 
\begin{align*}
&\bWonex\tran \bWtwox\tran \bWtwoz \bWonez \bz = \\
& \hspace{0.5in} \underbrace{\bWonex\tran [ \bWtwox\tran \bWtwoz - \bQ_{\bxone,\bzone} ]\bWonez \bz}_{T_1} \\
& \hspace{0.5in} + \underbrace{	{\sst \left[\frac{\pi - \ttheta_j^{(1)} }{2\pi} \right]_{j=1}^{D_1} }\odot \bWonex\tran \bWonez \bz}_{T_2} \\
& \hspace{0.5in} + \underbrace{ {\sst \left[\frac{\sin \ttheta_j^{(1)} \|\tz_j^{(1)}\|_2}{2\pi \|\tx_j^{(1)}\|_2} \right]_{j=1}^{D_1} } \odot \bWonex\tran \bWonex \bx}_{T_3}.
\end{align*}
We have 
\begin{align}\label{eq:T1}
&|T_1| \leq \|\bWonex\| \|\bWonez\| \|\bWtwox\tran \bWtwoz - \bQ_{\bxone,\bzone} \| \|\bz\|_2 \leq \eps \|\bz\|_2
\end{align}
where we used \eqref{eq:normbound2} and Lemma~\ref{lem:Central}. Expanding $T_2$ we get
\begin{align*}
&T_2 - {\sst \left[\frac{\pi - \ttheta_j^{(1)}}{2\pi} \right]_{j=1}^{D_1} } \odot \bQxz \bz = {\sst \left[\frac{\pi - \ttheta_j^{(1)}}{2\pi} \right]_{j=1}^{D_1} } \odot [\bWonex\tran \bWonez \bz - \bQxz]
\end{align*}
which can be bounded as follows
\begin{align}\label{eq:T2}
&\left| T_2 -  {\sst \left[\frac{\pi - \ttheta_j^{(1)}}{2\pi} \right]_{j=1}^{D_1} } \odot \bQxz \bz \right| \leq \|\bWonex\tran \bWonez \| \|\bz\|_2 \leq \eps \|\bz\|_2
\end{align}
where we used \eqref{eq:central}.
Using the definition \eqref{eq:Qxz} of $\bQxz$ we observe that
\begin{align}\label{eq:Qxzbz}
\bQxz \bz = {\sst \left[\frac{\pi - \theta_j^{(0)}}{2\pi} \right]_{j=1}^{D_0} } \odot \bz + {\sst \left[\frac{\sin \theta_j^{(0)} \|z_j^{(0)}\|_2}{2\pi \|x_j^{(0)}\|_2} \right]_{j=1}^{D_0} } \odot \bx.
\end{align}
Expanding $T_3$ we get
\begin{align}\label{eq:T3}
&\left| T_3 - {\sst \left[\frac{\sin \ttheta_j^{(1)} \|\tz_j^{(1)}\|_2}{2\pi \|\tx_j^{(1)}\|_2} \right]_{j=1}^{D_1} }\odot \bx \right| = \notag \\
&\hspace{0.3in} \left| {\sst \left[\frac{\sin \ttheta_j^{(1)} \|\tz_j^{(1)}\|_2}{2\pi \|\tx_j^{(1)}\|_2} \right]_{j=1}^{D_1} } \odot [\bWonex\tran \bWonex - \bI] \bx \right| \leq \notag \\
&\hspace{0.3in} \|\bWonex\tran \bWonex - \bI\| \|\bx\|_2 \leq \eps \|\bx\|_2
\end{align}
where we used \eqref{eq:central2} in the last inequality. The result follows by combining \eqref{eq:T1}, \eqref{eq:T2}, \eqref{eq:Qxzbz} and \eqref{eq:T3}. 

\end{proof}

\input{part1}



\bibliographystyle{plainnat}
\bibliography{../references}

\end{document}
