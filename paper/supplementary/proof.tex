%!TEX root = supplementary.tex

\section{Proof for Theorem~\ref{thm:injective}}\label{sec:proof}
\begin{figure}[h]
\centering
\begin{minipage}{0.8\linewidth}
\newcommand{\figWidth}{ 0.45\linewidth } 
% \def\arraystretch{3}    
% \setlength\tabcolsep{0.2mm} 
\begin{tabular}{ c c }
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{../figures/matrix1.pdf} \\
  (a)
  \end{minipage}
  & 
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{../figures/matrix2.pdf} \\
  (b)
  \end{minipage}
\end{tabular}
\end{minipage}
\caption{Illustration of a single transposed convolution operation. $k_i^j$ stands for $i^{th}$ filter kernel for the $j^{th}$ input channel. $z$ and $x$ denote the input and output signals, respectively. (a) The standard transposed convolution represented as linear multiplication. (b) With proper row and column permutations, the permuted weight matrix has a repeating block structure.}
\label{fig:deconMatrix}
\end{figure}

%Due to space limits, we only give an outline of the proof and prove crucial lemmas. Further details can be found in %the supplementary document \cite{supplementary}. 
Proof mostly follows the arguments in the recent paper by \cite{hand2017global}. As we discussed in Section~\ref{sec:problem}, the weight matrix $W_1 \in \R^{C_1D_1 \times C_0 D_0}$ of the first layer of the network can be arranged as a block matrix $\bW_1 = [W]_{i=1}^{D_0}$ where $W \in \R^{C_1 \ell \times C_0}$ is a Gaussian matrix repeating in each block, see \prettyref{fig:deconMatrix}. In the rest of the proof we will use this arrangement of the matrix. Note that this effectively means a permuation of the vectors after each layer. This has to be handled carefully througout the proof.
 %and this is where our proof departs from the proof of \cite{hand2017global}. 
By assumption, the sampling matrix $A$ is an identity matrix, so the cost function can be written as 
$$
J(\bz) = \frac{1}{2} \| G(\bztrue) - G(\bz) \|_2^2.
$$
The operation of the first layer on an input signal $\bz = [z_i]_{i=1}^{D_0}$ is 
$$
\bWonez \bz = \si (\bW_1 \bz).
$$
In general operation of the generator network can be written as
$$
G(\bz) = \bWtwoz \bWonez \bz.
$$

\begin{rem}
The matrix $\bWonez$ captures the operation of ReLU activation combined with weights of each layer, hence it will be instrumental in the rest of the proof. We note that that in the case of Leaky ReLU activation $L(x) = \left\{ \begin{array}{ll} x \ \ \text{if } x \geq 0 \\ \alpha x \ \ \text{if } x<0 \end{array} \right.$, the output of each layer is
\begin{align*}
L (\bW_1 \bz) &= \diag(\bW_1 \bz > 0) \bW_1 \bz + \alpha \ \diag(\bW_1 \bz < 0) \bW_1 \bz = \alpha \bW_1 \bz + (1-\alpha) \bWonez  \bz.
\end{align*}
\end{rem}

In the rest of the proof we assume the input vectors are in the block form as well as the weight matrices and denote them in boldface. This does not change the operation of the neural network. Next we prove a  central technical lemma which concerns concentration of the matrix 
$\bWonex\tran \bWonez$. First we define the following matrices. 
For any nonzero $x,z \in \R^n$ with angle $\theta_{x,z} = \angle(x,z)$ between , let
\begin{align}\label{eq:Qxz}
Q_{x,z} :=  \frac{\pi - \theta_{x,z}}{2\pi} \Id_n + \frac{\sin \theta_{x,z}}{2\pi} \Mxz. 
\end{align}
Similarly for two block vectors $\bx = [x_i]_{i=1}^n$ and $\bz = [z_i]_{i=1}^n$, define the block matrix
\begin{align}\label{eq:Qxz2}
\bQxz := [Q_{x_i,z_i}]_{i=1}^n. 
\end{align}

The following result appears in \cite{hand2017global}. 

\begin{lem}[Lemma~5~\cite{hand2017global}]\label{lem:HandConcentrate}
Fix $\eps \in (0,1)$. Let $A \in \R^{n \times k}$ have i.i.d. ${\cal N}(0,1/n)$ entries. If $n > c k \log k$, then with probability at least $1- 8 n e^{-\gamma k}$,
\begin{align}\label{eq:Aconc}
\forall x,z \in \R^k, \|A_{+,x}\tran A_{+,z} - Q_{x,z}\| \leq \eps.
\end{align}
When $x = y$, it holds
\begin{align}\label{eq:Aconc2}
\forall x \neq 0, \|A_{+,x}\tran A_{+,x} - \Id_n /2\| \leq \eps.
\end{align}
Here $c,\gamma$ depends only on $\eps$.
\end{lem}
Here the matrix $Q_{x,z}$ happens to be the expectation of the matrix $A_{+,x}\tran A_{+,z}$. This can be shown by an elementary calculation. We can now state the central technical lemma. 

\begin{lem}\label{lem:Central}
Fix $\eps \in (0,1)$. Let $\bW = [W]_{i=1}^{D_0}$ where $W \in \R^{C_1 \ell \times C_0}$ have i.i.d. ${\cal N}(0,1/{C_1 \ell})$. If $C_1 \ell \gtrsim_\eps C_0 \log C_0$, then with probability at least $1 - 8 D_0 \ell C_1 \ e^{-\gamma C_0}$,
\begin{align}\label{eq:central}
\forall \bx,\bz \in \R^{C_0D_0}, \|\bWx\tran \bWz - \bQxz\| \leq \eps.
\end{align}
When $x = y$, it holds
\begin{align}\label{eq:central2}
\forall \bx \neq 0, \|\bWx\tran \bWx - \bI /2\| \leq \eps.
\end{align}
Here $c,\gamma$ depends only on $\eps$.
\end{lem}
Lemma~\ref{lem:Central} is crucial to the rest of the proof. We note that the conditions of Theorem~\ref{thm:injective} are almost identical to the ones of Lemma~\ref{lem:Central}. In other words, the concentration of weight matrices as given in \eqref{eq:neighbor} and \eqref{eq:localMax} are enough to imply the existence of a strict descent direction for the cost function $J(x)$ outside of two small neighborhoods. We now prove the lemma.

\begin{proof}
Observe that for block vectors $\bx = [x_i]_{i=1}^{D_0}$ and $\bz = [z_i]_{i=1}^{D_0}$ we can write 
\begin{align}\label{eq:block}
\bWonex\tran \bWonez = [W_{+,x_i}\tran W_{+,z_i}]_{i=1}^{D_0}. 
\end{align}
 We know from spectral norm of block matrices that
\begin{align}
\|\bWonex\tran \bWonez - \bQxz\| = \max_{i=1,\ldots,D_0} \|W_{+,x_i}\tran W_{+,z_i} - Q_{x_i,z_i} \|. \label{eq:blocknorm}
\end{align}
Lemma~\ref{lem:HandConcentrate} implies that if $C_1 \ell \gtrsim C_0 \log C_0$ then with probability at least $1 - 8 C_1 \ell e^{-\gamma C_0}$, it holds that $\|W_{+,x_i}\tran W_{+,z_i} - Q_{x_i,z_i} \| \leq \eps$. 
A union bound argument yields 
\begin{align*}
&\P(\|\bWonex\tran \bWonez - \bQxz\|  \leq \eps) \leq \sum_{i=1}^{D_0} \P(\|W_{+,x_i}\tran W_{+,z_i} - Q_{x_i,z_i} \|  \leq \eps) \leq 8 D_0 C_1 \ell \ e^{-\gamma C_0}.
\end{align*}
\end{proof}
Next, we present another useful result that controls how $\bWonex$ distorts the angle between two vectors $\bx,\bz$. 
%Formally we define the angle vector $\theta_{\bx,\bz} = [\angle(x_i,z_i)]_{i=1}^n$, where $n$ is number of the blocks. 
Let:
$$
g(\theta) := \cos^{-1} \left( \frac{(\pi - \theta) \cos \theta + \sin \theta}{\pi} \right).
$$
First we borrow another result from \cite{hand2017global}. 
\begin{lem}[Lemma~23~\cite{hand2017global}]\label{lem:angleConcentrate}
Fix $\eps \in (0,0.1)$. Let the conditions of Lemma~\ref{lem:HandConcentrate} hold and $A$ satisfy \eqref{eq:Aconc}. For $x,z$ denote $\theta_0 = \angle(x,z)$ and $\theta_1 := \angle(A_{+,x} x, A_{+,z} z)$. Then
$$
|\theta_1 - g(\theta_0)| \leq 4\sqrt{\eps}.
$$
\end{lem}
This lemma shows that a Gaussian matrix combined with ReLU operation preserves the angle between vectors up to function $g(\cdot)$. Our next lemma uses this result.
\begin{lem}\label{lem:lowerbound}
Fix $ \eps < 1/(16 \pi)^2$. Assume that weight matrices $\bW_1$ and $\bW_2$ satisfy \eqref{eq:central} with constant $\eps$. Then it holds for all $\bx, \bz \neq 0$ that
$$
\la \bWtwox \bWonex \bx, \bWtwoz \bWonez \bz \ra > 0.
%\frac{1}{16\pi} \|\bx\|_2 \|\bz\|_2.
$$
\end{lem}
% We give the proof of this lemma in the supplementary material\cite{supplementary} due to lack of space here. We note that the proof uses Lemma~\ref{lem:Central} and Lemma~\ref{lem:angleConcentrate}. 

\begin{proof}

We operate under the assumptions that the weight matrices $W_j$ for layer $i=1,2$ satisfy \prettyref{eq:Aconc2} and \prettyref{eq:central2}. In particular these equations imply that for layer $i=1,2$ and all input vector $z,\bz \neq 0$, 
\begin{align}
\frac{1}{2} - \eps  \leq \| W_{i,+,z}\|^2 &\leq \frac{1}{2} + \eps \label{eq:normbound1} \\
\frac{1}{2} - \eps \leq \| \bW_{i,+,\bz}\|^2 &\leq \frac{1}{2} + \eps \label{eq:normbound2}.
\end{align}
Since $\bz^{(1)} = \bWonez \bz^{(0)}$, it follows that for all blocks $j = 1,\ldots, D_0$ that 
\begin{align}
&\sqrt{\frac{1}{2} - \eps} \ \|z_j^{(0)}\|_2 \leq \|z_j^{(1)}\|_2 \leq \sqrt{\frac{1}{2} + \eps} \ \|z_j^{(0)}\|_2 \label{eq:awayzero} \\
&\sqrt{\frac{1}{2} - \eps} \ \|\bz^{(0)}\|_2 \leq \|\bz^{(1)}\|_2 \leq \sqrt{\frac{1}{2} + \eps} \ \|\bz^{(0)}\|_2. \notag
\end{align}
The same statements hold true for $\bx$ and $x_j^{(i)}$ as well. Consequently, by dividing the inequalities we have for $\bx$ and $\bz$ that 
\begin{align}
&\sqrt{\frac{1-2\eps}{1+2\eps}} \ \frac{\|x_j^{(0)}\|_2}{\|z_j^{(0)}\|_2} \leq \frac{\|x_j^{(1)}\|_2}{\|z_j^{(1)}\|_2} \leq \sqrt{\frac{1+2\eps}{1-2\eps}} \ \frac{\|x_j^{(0)}\|_2}{\|z_j^{(0)}\|_2} \label{eq:vectorratio} \\
&\sqrt{\frac{1-2\eps}{1+2\eps}} \ \frac{\|\bx^{(0)}\|_2}{\|\bz^{(0)}\|_2} \leq \frac{\|\bx^{(1)}\|_2}{\|\bz^{(1)}\|_2} \leq \sqrt{\frac{1+2\eps}{1-2\eps}} \ \frac{\|\bx^{(0)}\|_2}{\|\bz^{(0)}\|_2} . \label{eq:blockratio}
\end{align}

\begin{figure}[t]
\centering
\includegraphics[width=0.7\linewidth]{g_theta}
\caption{Plot of function $g(\cdot)$ defined in Section~\ref{sec:proof}}
\label{fig:g_theta}
\end{figure}

We assume without loss of generality that the input vectors $\bxo = [x_j^{(0)}]_{j=1}^{D_0}$ and $\bzo = [z_j^{(0)}]_{j=1}^{D_0}$ are block normalized, i.e., $\|x_j^{(0)}\|_2 = \|z_j^{(0)}\|_2 = 1$. We have
\begin{align}
&\la \bxone, \bzone \ra = \sum_j \la x_j^{(1)}, z_j^{(1)} \ra = \notag \\
&\|\bxone\|_2 \|\bzone\|_2 \cos \btheta^{(1)} = \sum_j \| x_j^{(1)} \|_2  \| z_j^{(1)} \|_2 \cos \theta_j^{(1)} \notag \\
&\geq \min_j \cos \theta_j^{(1)} \sum_j \| x_j^{(1)} \|_2  \| z_j^{(1)} \|_2. \label{eq:dotproduct}
\end{align}
Using the fact that input vectors are block normalized, it follows from the first inequality in \eqref{eq:vectorratio} that
\begin{align}\label{eq:left}
\sqrt{\frac{1-2\eps}{1+2\eps}} \|z_j^{(1)}\|_2^2 \leq \|x_j^{(1)}\|_2 \|z_j^{(1)}\|_2
\end{align}
and from the second inequality in \eqref{eq:blockratio} that
\begin{align}\label{eq:right}
 \|\bx^{(1)}\|_2 \|\bz^{(1)}\|_2 \leq \sqrt{\frac{1+2\eps}{1-2\eps}} \ \|\bz^{(1)}\|_2^2
\end{align}
Combining \eqref{eq:dotproduct}, \eqref{eq:left} and \eqref{eq:right} yields that
\begin{align*}
&\sqrt{\frac{1+2\eps}{1-2\eps}} \ \|\bz^{(1)}\|_2^2 \cos \btheta^{(1)} \geq \\
&\hspace{1.2in} \sqrt{\frac{1-2\eps}{1+2\eps}} \min_j \cos \theta_j^{(1)} \sum_j \|z_j^{(1)}\|_2^2 
\end{align*}
which in turn implies that
\begin{align}\label{eq:anglebound1}
(1 + 8 \eps) \cos \btheta^{(1)} \geq \min_j \cos \theta_j^{(1)} = \cos \theta_{\widehat{i}}^{(1)},
\end{align}
for some $\widehat{i}$. Here we used the fact that $\frac{1+v}{1-v} \leq 1+4v$ for $0 \leq z \leq \frac{1}{2}$. Lemma~\ref{lem:angleConcentrate} implies that $|\theta_{\widehat{i}}^{(1)} - g(\theta_{\widehat{i}}^{(0)})| \leq 4\sqrt{\eps}$. Since $g(\theta_{\hat{i}}^{(0)}) < \frac{\pi}{4}$ (see Figure~\ref{fig:g_theta}), we have $0 \leq \theta_{\widehat{i}}^{(1)} \leq \frac{\pi}{4} + 4\sqrt{\eps}$. Then for small enough $\eps$, \eqref{eq:anglebound1} implies that 
$$
\cos \btheta^{(1)} \geq  \frac {\cos \theta_{\widehat{i}}^{(1)}} {1 + 8 \eps} \geq \frac {\cos (\frac{\pi}{4} + 4\sqrt{\eps})} {1 + 8 \eps} > 0.6. 
$$
The last inequality comes from the fact that $\frac {\cos (\frac{\pi}{4} + 4\sqrt{\eps})} {1 + 8 \eps}$ is monotonically decreasing with small $\epsilon$, so the minimum can be computed and is roughly equals to $0.646$, since $\epsilon<1/(16\pi)^2$. The exact number doesn't affect the final results, because we only want to bound $\cos \btheta^{(1)}$ away from $0$.

Now we proceed to the second layer of the network. As explained in the \prettyref{sec:notation}, we first reorder vectors $\tbx^{(1)} = [\tx_j^{(1)}]_{j=1}^{D_1}$ and $\tbz^{(1)} = [\tz_j^{(1)}]_{j=1}^{D_1}$. We expand the dot product of $\bxone$ and $\bzone$ similarly as before
\begin{align*}
\|\bxone\|_2 \|\bzone\|_2 \cos \btheta^{(1)}  = \sum_j \| \tx_j^{(1)} \|_2  \| \tz_j^{(1)} \|_2 \cos \ttheta_j^{(1)}.
\end{align*}
Define the set of indices 
$$
I = \{ j: \| \tx_j^{(1)} \|_2 \neq 0 \text{ and } \| \tx_j^{(1)} \|_2 \neq 0 \}.
$$
Then we can continue
\begin{align*}
\|\bxone\|_2 \|\bzone\|_2 \cos \btheta^{(1)}  &= \sum_{j \in I} \| \tx_j^{(1)} \|_2  \| \tz_j^{(1)} \|_2 \cos \ttheta_j^{(1)} \\
&\leq \max_{j \in I} \cos \ttheta_j^{(1)} \sum_{j \in I} \| \tx_j^{(1)} \|_2  \| \tz_j^{(1)} \|_2 \\
&\leq \max_{j \in I} \cos \ttheta_j^{(1)} \|\tbx^{(1)}\|_2 \|\tbz^{(1)}\|_2 
\end{align*}
where we used Cauchy-Schwartz inequality in the last line. Since the reordering does not change the norm of the block vectors, we have $\|\bxone\|_2 \|\bzone\|_2 = \|\tbx^{(1)}\|_2 \|\tbz^{(1)}\|_2$. Consequently, it follows that $\cos \btheta^{(1)} \leq \max_{j \in I} \cos \ttheta_j^{(1)}$. In other words, for some $\widehat{j} \in I$,
\begin{align}\label{eq:anglelower}
0.6 < \cos \btheta^{(1)} \leq \cos \ttheta_{\widehat{j}}^{(1)}.
\end{align}
Recall that after the second transposed convolution layer of the network, we have 
$ z_j^{(2)} = W_2 \tz_j^{(1)}$. Since similar relation as in \eqref{eq:awayzero} holds for second layer as well and we have $\|\tx_{\widehat{j}}^{(1)}\|_2 > 0$ and $\|\tz_{\widehat{j}}^{(1)}\|_2 > 0$ as $\widehat{j} \in I$, it follows that
\begin{align}\label{eq:secondlayer}
\|\tx_{\widehat{j}}^{(2)}\|_2 > 0 \ \text{ and } \ \|\tz_{\widehat{j}}^{(2)}\|_2 > 0.
\end{align}
By invoking Lemma~\ref{lem:angleConcentrate} once again, we have $|\theta_{\widehat{j}}^{(2)} - g(\ttheta_{\widehat{j}}^{(1)})| \leq 4\sqrt{\eps}$. Combining this with \eqref{eq:anglelower} yields %\todo{should add more explanation}
\begin{align}\label{eq:finalangle}
\cos \theta_{\widehat{j}}^{(2)} \geq 0.6.
% \geq \cos\left(g\left(\ttheta_{\widehat{j}}^{(1)}\right) + 4\sqrt{\eps}\right) 
\end{align}
Finally we arrive at the desired result
\begin{align*}
&\la \bWtwox \bWonex \bxo, \bWtwoz \bWonez \bzo \ra \\
&= \la \bxtwo, \bztwo \ra = \sum_j \|x_j^{(2)}\|_2 \|z_j^{(2)}\|_2 \cos \theta_j^{(2)}  \\
& \geq \|x_{\widehat{j}}^{(2)}\|_2 \|z_{\widehat{j}}^{(2)}\|_2 \cos \theta_{\widehat{j}}^{(2)} > 0
\end{align*}
which follows from \eqref{eq:secondlayer} and \eqref{eq:finalangle}. 

%$\bigO(\eps)$\footnote{The big $\bigO$ notation}

\end{proof}









