%!TEX root = root.tex

\section{Experimental Validation}\label{sec:validation}

In this section, we verify the gaussian weight assumption of trained generative networks, our main result \prettyref{thm:sub_sampling} on simulated 2-layer networks, as well as the generalization of \prettyref{thm:sub_sampling} to more complex multi-layer networks trained on real datasets. 

\subsection{Gaussian Weight in Trained Networks}

\begin{figure}[ht]
\centering
\includegraphics[width=0.4\linewidth]{{{figures/validation/hist}}}
\caption{Distribution of the kernel weights from every layer in a trained convolutional generative network. The trained weights roughly follow a zero-mean gaussian distribution.}
\label{fig:gaussian-weight}
\end{figure}

We extract the convolutional filter weights, trained on real data to generate images in \prettyref{fig:mnist}, from a 4-layer convolutional generative models. The histogram of the weights in each layer is depicted in \prettyref{fig:gaussian-weight}. It can be observed that the trained weights highly resembles a zero-mean gaussian distribution. We also discover similar distributions of weights in other trained convolutional networks, such as ResNet~\cite{he2016deep}. \citet{AroraLM15} also report similar results.

\subsection{On 2-layer Networks with Random Weights}
As a sanity check on \prettyref{thm:sub_sampling}, we construct a generative neural network with 2 transposed convolution layers, each followed by a ReLU. The first layer has 16 channels and the second layer has 1 single channel. Both layers have a kernel size of $5$ and a stride of $3$. In order to be able to visualize the cost function landscape, we set the input latent space to be 2-dimensional. The weights of the transposed convolution kernels are drawn i.i.d. from a Gaussian distribution with zero mean and unit standard deviation. Only 50\% of the network output is observed. We compute the cost function $J(z)$ for every input latent code $z$ on a grid centered at the ground truth. The landscape of the cost function $J(z)$ is depicted in \prettyref{fig:geometry}(a). Although \prettyref{thm:sub_sampling} implies a possibility of a stationary point at the negative multiple of the ground truth, experimentally we do not observe convergence to any point other than the global minimum.

Despite the fact that \prettyref{thm:injective} and \prettyref{thm:sub_sampling} are proved only for the case of 2-layer network with ReLU, the same conclusion empirically extends to networks with more layers and different kernel sizes and strides. In addition, the inversion of generative models generalizes to other standard activation functions including Sigmoid, and Tanh. Specifically, Sigmoid and Tanh have quasi-convex landscapes as shown in \prettyref{fig:geometry}(b) and (c), which are even more favorable than that of ReLU. Leaky ReLU has the same landscape as a regular ReLU.
% \prettyref{fig:geometry} shows that under noisy measurements, the landscape is elevated but still has a similar geometry. The optimal solution (yellow triangle) is biased because of noise. 

\begin{figure}[htbp]
\centering
\begin{minipage}{\linewidth}
\newcommand{\figWidth}{ 0.24\linewidth } 
% \def\arraystretch{5}    
\setlength\tabcolsep{0.4mm} 
\begin{tabular}{ c c c c }
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{{{figures/validation/kernel=5-stride=3-channels=16-m=0.5}}}  \\
  (a) ReLU 
  \end{minipage}
  &
  % \begin{minipage}[m]{\figWidth}\centering
  % \includegraphics[width=\linewidth]{{{figures/validation/kernel=5-stride=3-channels=16-m=0.5-noise=0.05}}}  \\
  % (b) ReLU (noisy)
  % \end{minipage}
  % &
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{{{figures/validation/ac=sigmoid-kernel=5-stride=3-channels=16-m=0.49827-noise=0}}}  \\
  (b) Sigmoid 
  \end{minipage}
  &
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{{{figures/validation/ac=tanh-kernel=5-stride=3-channels=16-m=0.49827-noise=0}}}  \\
  (c) Tanh 
  \end{minipage}
  &
  \begin{minipage}[m]{\figWidth}\centering
  \vspace{2mm}
  \includegraphics[width=\linewidth,trim={0 0 0 1.5cm}]{{{figures/validation/mode_collapse}}}  \\
  (d) mode collapse
  \end{minipage}
\end{tabular}
\end{minipage}
\caption{The landscape of the cost function $J(z)$ for deconvolutional networks with (a) ReLU, (b) Sigmoid, and (c) Tanh as activation functions, respectively. There exists a unique global minimum.}
\label{fig:geometry}
\end{figure}

% \subsection{Example of Mode Collapse} 
As a counter example, 
% let us consider a 3-layer network with 2-dimensional latent space and 4, 8 and 16 filters. The kernel size and stride are fixed to be 3 and 2 across all layers. We observe the entire network output (i.e., no sub-sampling). 
we draw kernel weights uniformly randomly from $[0,1]$ (which violates the zero-mean Gaussian assumption). 
% , since a bounded distribution must have zero-mean to be sub-Gaussian
Consequently, there is a flat global minimum in the latent space, as shown in \prettyref{fig:geometry}(d). In this region, any two latent vectors are mapped to the exact same output, indicating that mode collapse indeed occurs.

\subsection{On Multi-layer Networks Trained with Real Data}
In this section, we demonstrate empirically that our finding holds for multi-layer networks trained on real data. The first network is trained with GAN to generate handwritten digits, and the second for celebrity faces. In both experiments, the correct latent codes can be recovered perfectly from partial (but sufficiently many) observations.

% \subsubsection{MNIST}\label{sec:mnist}
\paragraph{MNIST:}
For the first network on handwritten digit, we rescale the raw grayscale images from the MNIST dataset~\cite{lecun1998gradient} to size of $32 \times 32$. We used the conditional deep convolutional generative adversarial networks (DCGAN) framework~\cite{mirza2014conditional,radford2015unsupervised} to train both a generative model and a discriminator. Specifically, the generative network has 4 transposed convolutional layers. The first 3 transposed convolutional layers are followed by a batch normalization and a Leaky ReLU. The last layer is followed by a Tanh. The discriminator has 4 convolutional layers, with the first 3 followed by batch normalization and Leaky ReLU and the last one followed by a Sigmoid function. We use Adam with learn rate $0.1$ to optimize the latent code $z$ . The optimization process usually converges within 500 iterations. The input noise to the generator is set to have a relatively small dimension 10 to ensure a sufficiently expanding network.

\input{figureTex/mnist}

5 different sampling matrices are showcased in \prettyref{fig:mnist}, including observing uniform random samples, as well as the top half, bottom half, left half, and right half of the image space. In all cases, the input latent codes are recovery perfectly. We feed the recovered latent code as input to the network to obtain the completed image, shown in the $3^\text{rd}$ row. 

% \subsubsection{CelebFaces}
\paragraph{CelebFaces:}
\input{figureTex/celebA}
A similar study is conducted on a generative network trained on the CelebFaces~\cite{liu2015faceattributes} dataset. We rescale the raw grayscale images from the MNIST dataset~\cite{lecun1998gradient} to size of $64 \times 64$. A similar network architecture to previous MNIST experiment is adopted, but both the generative model and the discriminator have 4 layers rather than 3. The images are showcased in \prettyref{fig:celeba}. 

Note that the probability of exact recovery increases with the number of measurements. The minimum number of measurements required for exact recovery, however, depends on the network architecture, the weights, and the sampling spatial patterns. The mathematical characterization for minimal number of measurements remains a challenging open question.


