%!TEX root = root.tex

\section{Introduction}
In recent years, generative models have made significant progress in learning representations for complex and multi-modal data distributions, such as those of natural images~\cite{kingma2013auto,radford2015unsupervised}. However, despite the empirical success, there has been relatively little theoretical understanding into the mapping itself from the input latent space to the high-dimensional space.
%
% For instance, {\em generative adversarial networks} (GAN)~\cite{goodfellow2014generative,mirza2014conditional,radford2015unsupervised} and {\em variational auto-encoders} (VAE)~\cite{kingma2013auto} use deep neural networks to learn the mapping from a low-dimensional representation space to a high dimensional sample space. It is worth noting that the state-of-the-art generative models~\cite{karras2017progressive} use deep convolutional networks rather than fully-connected networks. These methods found success in producing realistic looking images. 
%
In this work, 
% we focus on a direction that is orthogonal to the current development of generative models. Rather than improving the quality of the produced images, 
we address the following question: given a convolutional generative network\footnote{Deep generative models typically use transposed convolution (\aka ``deconvolution''). With a slight abuse of notation we refer to transposed convolutional generative models as convolutional models.}, is it possible to ``decode'' an output image and recover the corresponding input latent code? In other words, we are interested in the {\em invertibility of convolutional generative models}. 

% The answers to such questions not only provide theoretical insights into convolutional generative networks, but also create a new paradigm for many challenging inference problems such as image classification. For instance, one can label an image simply by inverting a conditional generative model whose latent space has clear physical interpretations that relate to the labels. Another example is the case where only a small portion of the generative model's output is observed, as illustrated in \prettyref{fig:network}, which found applications in image reconstruction and image inpainting~\cite{yeh2017semantic,pathak2016context}. 

\begin{figure}[t]
\centering
\includegraphics[width=0.4\linewidth]{figures/architecture.pdf}
\caption{Recovery of the input latent code $z$ from under-sampled measurements $y=A G(z)$ where $A$ is a sub-sampling matrix and $G$ is an expanding generative neural network. We prove that $z$ can be recovered with guarantees using simple gradient-descent methods under mild technical assumptions. 
% Best viewed in color.
}
\label{fig:network}
\end{figure}

The impact of the network inversion problem is two-fold. Firstly, the inversion itself can be applied in image in-painting~\cite{yeh2017semantic,pathak2016context}, image reconstruction from sparse measurements~\cite{ma2017sparse,ma2017sparse2dense}, and image manipulation~\cite{zhu2016generative} (\eg, vector arithmetic of face images~\cite{liu2015faceattributes}). Secondly, the study of network inversion provides insight into the mapping from the low-dimensional latent space to the high-dimensional image space (\eg, is the mapping one-to-one or many-to-one?). A deeper understanding of the mapping can potentially help solve the well known mode collapse\footnote{Mode collapse refers to the problem that the Generator characterizes only a few images to fool the discriminator in GAN. In other words, multiple latent codes are mapped to the same output in the image space.} problem~\cite{srivastava2017veegan} during the training in the generative adversarial network (GAN)~\cite{goodfellow2014generative,mirza2014conditional}.

The challenge of the inversion of a deep neural network lies in the fact that the inversion problem is highly non-convex, and thus is typically computationally intractable and without optimality guarantees. However, in this work, we show that network inversion can be solved efficiently and optimally, despite being highly non-convex. Specifically, we prove that with simple first-order algorithms like stochastic gradient descent, we can recover the latent code with guarantees. The sample code is
available at \texttt{https://github.com/fangchangma/invert-generative-networks}.
% give a global analysis for inverting a convolutional generative network.
% This paradigm differs from the current mainstream methods which train discriminative models specifically for classification tasks.



% The inversion of a deep neural network, as well as the optimization of its weights, is a highly non-convex problem. Despite the non-convex nature of the objective functions, simple first-order algorithms like stochastic gradient descent and its variants often produce desirable results. This is because usually the landscape of objective functions usually have a favorable geometry despite being non-convex, such as having no spurious local minima. Also there are recent works in solving non-convex inverse problems \cite{GeLeeMa2016,Banderia2016}. 

\subsection{Related Work}
%
The network inversion problem has attracted some attention very recently. For instance, \citet{bora2017compressed} empirically find that minimizing the non-convex \prettyref{pro:1}, which is defined formally in \prettyref{sec:problem}, using standard gradient-based optimizer yields good reconstruction results from small number of Gaussian random measurements. They also provide guarantees on the global minimum of a generative network with certain structure. However, their work does not analyze how to find the global minimum. \citet{hand2017global} further establish that a fully connected generative network with weights following Gaussian distribution can be inverted given only compressive linear observations of its last layer. In particular, they show that under mild technical conditions \prettyref{pro:1} has a favorable global geometry, in the sense that there are no stationary points outside of neighborhoods around the desired solution and its negative multiple with high probability. However, most practical generative networks are deconvolutional rather than fully connected, due to memory and speed constraints. Besides, their results are proved for Gaussian random measurements, which are rarely encountered in practical applications. In this work, we build on top of \cite{hand2017global} and extend their results to 2-layer deconvolutional neural networks, as well as uniform random sub-sampling.
% These limitations motivate us to study the landscape of deconvolutional neural networks and show theoretical guarantees for gradient descent approach to find the global minimum. 
%generalize \citet{hand2017global}'s results to deconvolutional generative models. 
% {\color{red}We are doubtful regarding this constant $\pi$, since it does not match our experimental observations.}

% \todo{@Fangchang:\citet{AroraLM15} (Gaussian weights) - show experiments about the distribution of weights?}

%\todo{@Ulas:\citet{gilbert2017towards} (The badly written paper)}
We also note the work \cite{gilbert2017towards}, which studies a 1-layer network with a special activation function (Concatenated ReLU, which is essentially linear) and a strong assumption on the latent code ($k$-sparsity). In comparison, our results are much stronger than \cite{gilbert2017towards}. Specifically, our results are for 2-layer networks (with empirical evidences for deeper networks), and they apply to the most common ReLU activation function. Our result also makes no assumption regarding the sparsity of latent codes. 
% However, their assumption on the input signal is different than ours, and their methods appear not able to handle nonlinearities such as ReLUs.

Another line of research, which focuses on gradient-based algorithms, analyzes the behavior of (stochastic) gradient descent for Gaussian-distributed input. \citet{Soltan2017} showed that projected gradient descent is able to find the true weight vector for 1-layer, 1-neuron model. More recently, \citet{Du2017} improved this result for a simple convolutional neural network with two unknown layers. Their assumptions on random input and their problem of weight learning are different than the problem we study in this paper. 

% \paragraph{Compressive Sensing} 
Our problem is also connected to {\em compressive sensing} \cite{Donoho04,carota06} which exploits the sparsity of natural signals to design acquisition schemes where the number of measurements scales linearly with the sparsity level. The signal is typically assumed to be sparse in a given dictionary, and the objective function is convex.
 % ($\ell_1$-minimization). 
In comparison, our work does not assume sparsity, and we provide a direct analysis of gradient descents for the highly non-convex problem. 
% Specifically, we exploit the hierarchical nature of images and other natural signals by leveraging the powerful representation capability of deep learning.

% \paragraph{Image Inpainting}  empirically show that GAN can be applied in image inpainting. They optimize the latent encoding using a weighted combination of a $\mathcal{L}_2$ loss and the discriminator loss. The inpainting results are great, but there are no theoretical guarantees and the inpainting process still depends on using the discriminator.

% \cite{pathak2016context} Contextual Encoder


\subsection{Contribution} 
The contribution of this work is three-fold:
\begin{itemize}
  \item We prove that a convolutional generative neural network is invertible, with high probability, under the following assumptions: (1) the network consists of two layers of transposed convolutions followed by ReLU activation functions; (2) the network is (sufficiently) expansive; (3) the filter weights follow a Gaussian distribution. When these conditions are satisfied, the input latent code can be recovered from partial output of a generative neural network by minimizing a $\mathcal{L}_2$ empirical loss function using gradient descent. 
  % We discuss how we can extend this theorem to multiple networks and Leaky ReLU, but defer them to future work.
  \item We prove that the same inversion can be achieved with high probability, even when only a subset of pixels is observed. This is essentially the image inpainting problem.
  \item We validate our theoretical results using both random weights and weights trained on real data. We further demonstrate empirically that that our theoretical results generalize to (1) multiple-layer networks; (2) networks with other nonlinear activation functions, including Leaky ReLU, Sigmoid and Tanh. 
  % \item By using conditional GANs, we infer on latent code space to have meaningful information about the images 
  %  We demonstrate two simple use cases of generative neural network inversion. Specifically, we show that such technique can be applied in handwritten digit recognition and gender prediction from facial images. To our best knowledge, this is the first time to use a generator network to perform inference tasks on images. The very same principle can be applied in more complicated tasks with more complex networks and more training data.  
\end{itemize}

%\subsection{Key technical ideas}
Two key ideas of our proof include (a) the concentration bounds of convolutional weight matrices combined with ReLU operation, and (b) the angle distortion between two arbitrary input vectors under the transposed convolution and ReLU. In general, our proof follows a similar basic structure to \cite{hand2017global}, where the authors show the invertibility of fully connected networks with Gaussian weights. However, in fully connected networks, the weight matrix of each layer is a dense Gaussian matrix. In contrast, in convolutional networks the weight matrices are highly sparse with block structure due to striding filters, as in \prettyref{fig:deconMatrix}(a). Therefore, \cite{hand2017global}'s proof does not apply to convolutional networks, and the extension of concentration bounds for our case is not trivial. 

To address such problem, we propose a new permutation technique which shuffles the rows and columns of weight matrices to obtain a block matrix, as depicted in \prettyref{fig:deconMatrix}(b). With permutation, each block is now a dense Gaussian matrix, where we can apply existing matrix concentration results. However, the permutation operation is quite arbitrary, depending on the structure of the convolutional network. This requires some careful handling, since the second step (b) requires the control of angles. 

In addition, \citet{hand2017global} assume a Gaussian sub-sampling matrix at the output of the network, rather than partial sub-sampling (sub-matrix of identity matrix) that we study in this problem. We observe that sub-sampling operation can be swapped with the last ReLU in the network, since both are entrywise operations. We handle the sub-sampling by making the last layer more expansive, and prove that it is the same with no downsampling from a theoretical standpoint. 





