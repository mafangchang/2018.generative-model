import argparse
import os.path
import numpy as np
import csv
import sys
from pprint import pprint

import matplotlib.pyplot as plt 
from matplotlib.ticker import FormatStrFormatter
colormap = plt.cm.gist_ncar

percs = []
errors = []
accs = []
with open("test.csv", 'r') as csvfile:   
    reader = csv.DictReader(csvfile)
    for row in reader:
        percs.append(float(row["perc"]))
        errors.append(float(row["error"]))
        accs.append(100 - float(row["error"]))

def plot_and_save(x, y, xlabel, ylabel, filename):
    fig, ax = plt.subplots()
    fig.set_size_inches(5, 2)
    # majorFormatter = FormatStrFormatter('%.3f')
    # ax.yaxis.set_major_formatter(majorFormatter)
    plt.rcParams.update({'font.size': 12})

    # ax.grid() # grid on

    plt.plot(x, y, linestyle='--', marker='o') # '--bo'
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    # plt.title(title)

    # save figure
    fig.savefig(filename, format='pdf', bbox_inches='tight', dpi=300)

def main():
    plot_and_save(percs, accs, "percentage of samples [%]", 'accuracy [%]', 'accuracy_vs_perc.pdf')


if __name__ == '__main__':
    main()
