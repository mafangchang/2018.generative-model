%!TEX root = root.tex

\section{Problem Statement}
\label{sec:problem}

In this section, we introduce the notation and define the network inversion problem. Let $\ztrue \in \R^{n_0}$ denote the latent code of interest, $G(\cdot): \R^{n_0} \to \R^{n_d} \, (n_0 \ll n_d)$ be a $d$-layer generative network that maps from the latent space to the image space. Then the ground truth output image $\xtrue \in \R^{n_d}$ is produced by 
\begin{align}\label{eq:image}
\xtrue = G(\ztrue),
\end{align}
In this paper we consider $G(\cdot)$ to be a deep neural network\footnote{Note that this network inversion problem happens at the inference stage, and thus is independent of the training process.}. In particular we assume $G(\cdot)$ to be a two-layer transposed convolutional network, modeled by 
\begin{align}\label{generator}
G(z) = \si (W_2 \si (W_1 z))
\end{align}
where $\si(z) = \max(z,0)$ denotes the rectified linear unit (ReLU) that applies entrywise. $W_1 \in \R^{n_1 \times n_0}$ and $W_2 \in \R^{n_2 \times n_1}$, are the weight matrices of the convolutional neural network in the first and second layers, respectively. Note that since $G(\cdot)$ is a convolutional network, $W_1$ and $W_2$ are highly sparse with a particular block structure, as illustrated in \prettyref{fig:deconMatrix}(a).

Let us make the inversion problem a bit more general by assuming that we only have partial observations of the output image pixels. Specifically, let $A \in \R^{m \times n_2}$ be a sub-sampling matrix (a subset of the rows of an identity matrix), and then the observed pixels are $\ytrue = A \xtrue \in \R^m$. Consequently, the inversion problem given partial measurements can be described as follows:
\begin{align*}
&\text{Let:} \ \ \ \ \ztrue \in \R^{n_0}, W_1 \in \R^{n_1 \times n_0}, W_2 \in \R^{n_2 \times n_1}, A \in \R^{m \times n_2}  \\
&\text{Given:}  \ A, W_1, W_2 \text{ and observations } \ytrue = A G(\ztrue) \\
&\text{Find:} \ \ \ \ztrue \text{ and } \xtrue = G(\ztrue)
\end{align*}

% The problem above aims to recover $\xtrue$ from the its compressed measurements $\ytrue$, given the information that $\xtrue$ lies in the range of a generative neural network. 
Since $\xtrue$ is determined completely by the latent representation $\ztrue$, we only need to find $\ztrue$. We propose to solve the following optimization problem for an estimate $\hat{z}$:
\begin{align}\label{pro:1}
\hat{z} = \arg\min_z J(z), \mbox{ where } J(z) = \frac 1 2 \norm{\ytrue - AG(z)}^2
\end{align}
% where
% \begin{align}\label{eq:cost}
% J(z) = \frac 1 2 \norm{\ytrue - AG(z)}^2.
% \end{align}

This minimization problem is highly non-convex because of $G$. Therefore, in general 
a gradient descent approach is not guaranteed to find the global minimum $\ztrue$, where $J(\ztrue)=0$. 

\subsection{Notation and Assumptions}\label{sec:network}
% In this work, we assume $G$ to be deconvolutional neural network with a certain structure given in \prettyref{fig:deconMatrix}. 
% \begin{align}
% G(z) = \si (W_2 \si (W_1 z)),
% \end{align}
% where $*$ denotes convolution, $F_i = \{f_{i,1}, \dots, f_{i,C_i}\}$ is a bank of $C_i$ filters at the $i^{th}$ layer, each with kernel size $l$. In other words, $f_{i,j} \in \R^\ell$ for $j \in \{1, \dots, C_i\}$. 

\begin{figure}[h]
\centering
\begin{minipage}{0.8\linewidth}
\newcommand{\figWidth}{ 0.45\linewidth } 
% \def\arraystretch{3}    
% \setlength\tabcolsep{0.2mm} 
\begin{tabular}{ c c }
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{figures/matrix1.pdf} \\
  (a)
  \end{minipage}
  & 
  \begin{minipage}[m]{\figWidth}\centering
  \includegraphics[width=\linewidth]{figures/matrix2.pdf} \\
  (b)
  \end{minipage}
\end{tabular}
\end{minipage}
\caption{Illustration of a single transposed convolution operation. $f_{i,j}$ stands for $i^{th}$ filter kernel for the $j^{th}$ input channel. $z$ and $x$ denote the input and output signals, respectively. (a) The standard transposed convolution represented as linear multiplication. (b) With proper row and column permutations, the permuted weight matrix has a repeating block structure.}
\label{fig:deconMatrix}
\end{figure}

We vectorize the input signal to 1D signal. 
% We assume the input signal $z \in \R^{n_0} $ consists of $C_0$ channels, each of size $D_0$, i.e, $n_0 = C_0 D_0$. 
The feature at the $i^{th}$ layer consists of $C_i$ channels, each of size $D_i$. Therefore, $n_i = C_i \cdot D_i$.
% For each input channel, $i \in \{1,\ldots,C_0\} $, let $f_{i,j}$ for $ j \in \{1,\ldots,C_1\}$ denote one of $C_1$ filters, each of size $\ell$. 
At any convolutional layer, let $f_{i,j}$ denotes the kernel filter (each of size $\ell$) for the $i^{th}$ input channel and the $j^{th}$ output channel. For simplicity, we assume the stride to be equal to the kernel size $l$.
% We assume that each filter $f_{i,j}$ is a vector of size $\ell$ with i.i.d. Gaussian entries from ${\cal N}(0,1/C_1\ell)$. Let $s$ be the stride size, the number of indices by which we shift each filter. 
All filters can be concatenated to form a large block matrix $W_i$. For instance, an example of such block matrix $W_1$ for the first layer is shown in \prettyref{fig:deconMatrix}(a).
% 
% The number of shifts for each input channel is $D_0$, same as the size of the input channel. 
% % For the sake of computational ease, 
% Without loss of generality and for the sake of simplicity, we assume that the stride size $s$ in each block of $W_1$ is as large as the filter size $\ell$, i.e., $s = \ell$. 
% 
Under our assumptions, the input and output sizes at each deconvolution operation can be associated as $D_{i+1} = D_i  \ell$.
 % and the overall output vector $x =W_1 z $ is of size $n_1 = C_1 D_1$. The assumption made on the stride size becomes handy for our proof technique in later sections.

Let $D_vJ(x)$ be one-sided directional derivative of the objective function $J(\cdot)$ along the direction $v$, \ie, $D_vJ(x) = \lim_{t \to 0^+} \frac{J(x+tv) - J(x)}{t}$. Let ${\cal B}(x, r)$ be the Euclidean ball of radius $r$ centered at $x$. We omit some universal constants in the inequalities and use $\gtrsim_\eps$ (if the constant depends on a variable $\eps$) instead. 



