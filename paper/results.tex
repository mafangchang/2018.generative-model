%!TEX root = root.tex

%%%%%%%%%%%%%% MAIN RESULT %%%%%%%%%%

\section{Main Results}

In this section, we present our main theoretical results regarding the invertibility of a 2-layer convolutional generative network with ReLUs.
% modeled as deconvolutional neural network with two layers structured as the weight matrix described above with no bias term. 
Our first main theoretical contribution is as follows: although the problem in \eqref{pro:1} is non-convex, under appropriate conditions there is a strict descent direction everywhere, except in the neighborhood of $\ztrue$ and that of a negative multiple of $\ztrue$. 
\begin{theorem}[Invertibity of convolutional generative networks]\label{thm:injective}
Fix $\eps > 0$. Let $W_1 \in \R^{C_0D_0 \times C_1 D_1}$ and $W_2 \in \R^{C_1D_1 \times C_2 D_2}$ be deconvolutional weight matrices with filters in $\R^\ell$ with i.i.d. entries from ${\cal N}(0,1/C_i\ell)$ for layers $i = 1,2$ respectively. Let the sampling matrix $A = \Id$ be an identity matrix (meaning there's no sub-sampling). If $C_1 \ell \gtrsim_\eps C_0 \log C_0$ and $C_2 \ell \gtrsim_\eps C_1 \log C_1$ then with probability at least $1 - \kappa(D_1 C_1 \ e^{-\gamma C_0} + D_2 C_2 \ e^{-\gamma C_1})$ we have the following. For all nonzero $z$ and $\ztrue$, there exists $v_{z,\ztrue} \in \R^{n_0}$ such that 
\begin{align}
&D_{v_{z,\ztrue}} J(z) < 0,  &\forall z \notin {\cal B}(\ztrue, \eps \|\ztrue\|_2 ) \cup {\cal B}(-\rho \ztrue, \eps \|\ztrue\|_2 ) \cup \{ 0\} \label{eq:neighbor} \\
 &D_z J(0) < 0, &\forall z \neq 0, \label{eq:localMax}
\end{align}
where $\rho$ is a positive constant. Both $\gamma>0$ and $\kappa>0$ depend only on $\eps$. 
\end{theorem}
Theorem~\ref{thm:injective} establishes under some conditions that the landscape of the cost function is not adversarial. Despite the heavily loaded notation, Theorem~\ref{thm:injective} simply requires that the weight matrices with Gaussian filters should be sufficiently expansive (\ie, output dimension of each layer should increase by at least a logarithmic factor). Theorem~\ref{thm:injective} does not provide information regarding the neighborhood centered at $-\rho \xtrue$, which implies the possible existence of a local minimum or a saddle point. However, empirically we did not observe convergence to a point other than the ground truth. In other words, gradient descent seems to always find the global minimum, see Figure~\ref{fig:geometry}. 

One assumption we make is the size of stride $s$ being same as the filter size $\ell$. Although theoretically convenient, this assumption is not common in the practical choices of transposed convolutional networks. We believe a further analysis can remove this assumption, which we also leave as a future work. In practice different activation functions other than ReLU can be used as well, such as sigmoid function, Tanh and Leaky ReLU. It is also an interesting venue of research to see whether a similar analysis can be done with those activations. In particular, for Leaky ReLU we briefly explain how the proof would divert from ours in Section~\ref{sec:proof}. We include landscapes of the cost function when different activations are used in Figure~\ref{fig:geometry}.
% In particular, for the case of leaky ReLU function $f(x) = \left\{ \begin{array}{ll} x \ \ \text{if } x \geq 0 \\ \alpha x \ \ \text{if } x<0 \end{array} \right.$ , the 

Gaussian weight assumption might seem unrealistic at first. However, there is some research~\cite{AroraLM15} indicating that weights of some trained networks follow a normal distribution. We also make a similar observation on the networks we trained, see Section~\ref{sec:validation}. We also note that Theorem~\ref{thm:injective} does not require independence of network weights across layers. 

\begin{proofoutline} 
Due to space limitations, the complete proof of Theorem~\ref{thm:injective} is given in the supplementary material~\cite{supplementary}. Here we give a brief outline of the proof and highlight the main steps. The theorem is proven by showing two main conditions on the weight matrices. 

The first condition is on the spatial arrangement of the network weights within each layer. Lemma~\ref{lem:Central}~\cite{supplementary} provides a concentration bound on the distribution of the effective weight matrices (after merging the ReLUs into the matrices). It shows that the set of neuron weights within each layer are distributed approximately like Gaussian. A key idea for the proving Lemma~\ref{lem:Central} is our new {\em permutation} technique. Specifically, we rearrange both rows and columns of the sparse weight matrices, as in \prettyref{fig:deconMatrix}(a), into a block diagonal matrix, as in \prettyref{fig:deconMatrix}(b). Each block in the permuted matrix is the same Gaussian matrix with independent entries. 
% $C_1\ell \times C_0$ 
% Such a structure will be helpful in proving the main theorem. Both the first and the second layers are permuted. 
The permutation into block matrices helps turns each block in \prettyref{fig:deconMatrix}(b) into a dense Gaussian matrix, and therefore makes it possible to utilize existing concentration bounds on Gaussian matrices. 

The second condition is on the approximate angle contraction property of an effective weight matrix $W_i$ (after merging the ReLUs into the matrices). Lemma~\ref{lem:lowerbound}~\cite{supplementary} shows that the angle between two arbitrary input vectors $x$ and $y$ does not vanish under a transposed convolution layer and the ReLU. The permutation poses a significant challenge on the proof of Lemma~\ref{lem:lowerbound}, since permutation of the input vectors distorts the angles. The difficulty is handled carefully in the proof of Lemma~\ref{lem:lowerbound}, which deviates from the proof machinery in \cite{hand2017global} and hence is a major technical contribution. 
% 
% This proof can be extended to deeper networks with the same machinery. However, we leave it as a future work.
\end{proofoutline}

\begin{corollary}[One-to-one mapping]\label{cor:injective}
Under the assumptions of Theorem~\ref{thm:injective}, the mapping $G(\cdot): \R^{n_0} \to \R^{n_2} \, (n_0 \ll n_2)$ is injective (\ie, one-to-one) with high probability.
\end{corollary}

\prettyref{cor:injective} is a direct implication of \prettyref{thm:injective}. \prettyref{cor:injective} states that the mapping from the latent code space to the high-dimensional image space is one-to-one with high probability, when the assumptions hold. This is interesting from a practical point of view, because mode collapse is a well-known problem in training of GAN~\cite{srivastava2017veegan} and \prettyref{cor:injective} provides a sufficient condition to avoid mode collapses. It remains to be further explored how we can make use of this insight in practice.

\begin{conjecture}
Under the assumptions of Theorem~\ref{thm:injective}, let the network weights follow any zero-mean subgaussian distribution $\P(|x| > t) \leq c e^{-\gamma t^2}, \ \forall t>0$ instead of Gaussian. Then with high probability the same conclusion holds.
\end{conjecture}
A subgaussian distribution (\aka light-tailed distribution) is one whose tail decays at least as fast as a Gaussian distribution (\ie, exponential decay). This includes, for example, any bounded distribution and the exponential distribution. Empirically, we observe that Theorem~\ref{thm:injective} holds for a number of zero-mean subgaussian distributions, including uniform random weights and $\{+1, -1\}$ binary random weights.
% The same proof mechanism for Theorem~\ref{thm:injective} holds, because all concentration bounds also hold for subgaussian distributions.

Now let us move on to the case where the subsampling matrix $A$ is not an identity matrix. Instead, consider a fixed sampling rate $r \in (0,1]$.
\begin{theorem}[Invertibility under partial measurements]\label{thm:sub_sampling}
Under the assumptions of Theorem~\ref{thm:injective}, let $A \in \R^{m \times C_2D_2}$ be an arbitrary subsampling matrix with $m/(C_2D_2) \geq r $. Then with high probability the same result as Theorem~\ref{thm:injective} hold.
\end{theorem}
Note that the subsampling rate $r$ appears in the dimension of the weight matrix of the second layer. 
\begin{proof}
Since ReLU operation is pointwise, we have the identity
$$
y = A G(z) = A \si (W_2 \si (W_1 z)) = \si (A W_2 \si (W_1 z)).
$$
It suffices to show that Theorem~\ref{thm:injective} still holds with $A W_2$ as the last weight matrix. Note that $A W_2$ selects a row subset of the matrix $W_2$ \prettyref{fig:deconMatrix}(a). Consequently, after proper permutation, $A W_2$ is again a block diagonal matrix with each block being a Gaussian matrix with independent entries. Only this time the blocks are not identical, but instead have different sizes. As a result, Theorem~\ref{thm:injective} still holds for $A W_2$, since the proof of Theorem~\ref{thm:injective} does not require the identical blocks. However, there are certain dimension constraints, which can be met by expanding the last layer with a factor of $r$, the sampling rate. This modification is reflected in the additional dimension assumption on the weight matrix $W_2$.
\end{proof}

The minimal sampling rate $r$ is a constant that depends on both the network architecture (e.g., how expansive the networks are) and the sampling matrix $A$. We made 2 empirical observations. Firstly, 
% Empirically, 
spatially disperse sampling patterns (e.g., uniform random samples) require a lower $r$, whilst more aggressive sampling patterns (e.g., top half, left half, sampling around image boundaries) demand more measurements for perfect recovery. 
Secondly, regardless of the sampling patterns $A$, the probability of perfect recovery exhibits a phase transition phenomenon w.r.t. the sampling rate $r$. This observation supports Theorem 4 (i.e., network is invertible given sufficient measurements). 
A more rigorous and mathematical characterization of $r$ remains an open question.


% \subsection{Discussion}


% Our result assumes that the image of interest lies in the range of the generator. Obviously it is not possible to have a network that generates every natural image that we have. There has been research that look into the robustness properties for inverting the neural nets \cite{bora2017compressed} experimentally. It is of interest to extend our results and give a global analysis in the presence of noise and outliers. In practice, the strength of recovery results will partly depend on the expressivity power of the generator network of the natural images that we are interested in. In Section~\ref{sec:inference} we demonstrate experiments with real images and show that reconstruction does indeed work well with certain datasets trained with GANs. 
