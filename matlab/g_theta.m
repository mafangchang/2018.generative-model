close all
fontsize = 20;
theta = 0:0.01:pi;
g = acos(((pi-theta).*cos(theta) + sin(theta))/pi);
plot(theta,g,'LineWidth',3,'Color','r')
set(gca,'XTick',0:pi/4:pi) 
set(gca,'XTickLabel',{'0','\pi/4','\pi/2','3*\pi/4','\pi'}, 'fontsize',fontsize) 
set(gca,'YTick',0:pi/4:pi/2)
set(gca,'YTickLabel',{'0','\pi/4','\pi/2'}, 'fontsize',fontsize)
grid on
xlabel('\theta','FontSize',fontsize)
ylabel('g(\theta)','FontSize',fontsize)