from __future__ import print_function
import argparse
import torch
import torch.utils.data
from torch import nn, optim
from torch.autograd import Variable
from torchvision.utils import save_image

import os
import csv
import shutil
import time
import sys

import dataloader as nyu_dataset
from vae_metrics import AverageMeter, Result
from vae_models import NYU_VAE, ResNet_VAE

parser = argparse.ArgumentParser(description='VAE Example')
parser.add_argument('--model', default="simple", help='resnet | simple')
parser.add_argument('--nz', type=int, default=400, help='size of the latent z vector')
parser.add_argument('--nf', type=int, default=64)
parser.add_argument('--loss', default='l1')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('--batch-size', type=int, default=8, metavar='N',
                    help='input batch size for training (default: 8)')
parser.add_argument('--epochs', type=int, default=100, metavar='N',
                    help='number of epochs to train (default: 100)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=20, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--modality', default='rgbd', help='rgb | rgbd')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=10)
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('--lr', type=float, default=1e-3, help='learning rate, default=1e-3')
parser.add_argument('--gamma', type=float, default=1e-1, help='tradeoff between loss functions, default=1e-1')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()


torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


kwargs = {'num_workers': args.workers, 'pin_memory': True} if args.cuda else {}

bce = nn.BCELoss(size_average=True)
mse = nn.MSELoss(size_average=True)
l1 = nn.L1Loss(size_average=True)
if args.cuda:
    bce.cuda()
    mse.cuda()
def loss_function(recon_x, x, mu, logvar):
    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.mean(1 + logvar - mu.pow(2) - logvar.exp()) 
    # / torch.numel(x.data)
    # print("BCE={}, KLD={}".format(BCE.data[0], KLD.data[0]))

    if args.loss == 'bce':
        BCE = bce(recon_x, x)
        return BCE + args.gamma * KLD
    elif args.loss == 'mse':
        L2 = mse(recon_x, x)
        return L2 + args.gamma * KLD
    elif args.loss == 'l1':
        L1 = l1(recon_x, x)
        # print("l1={:.6f}, kld={:.6f}".format(L1.data[0], KLD.data[0]))
        return L1 + args.gamma * KLD
    else:
        print("invalid loss function")
        sys.exit(0)

fieldnames = ['mse', 'rmse', 'absrel', 'lg10', 'mae', 
                'delta1', 'delta2', 'delta3', 
                'batch_time', 'data_time', 'gpu_time', 'kld']
best_result = Result()
best_result.set_to_worst()

def main():
    global args, best_result, output_directory, train_csv, test_csv, best_txt

    if args.model == "resnet":
        args.nf=128
        image_size=256
        model = ResNet_VAE(args.modality, args.nz)
    elif args.model == "simple":
        image_size=128
        model = NYU_VAE(len(args.modality), args.nf, args.nz)
    else:
        print("invalid model choice")
        sys.exit(0)

    if args.cuda:
        model.cuda()

    output_directory = os.path.join('vae_results', 'modality={}.model={}.loss={}.nz={}.nf={}.bs={}.lr={}.gamma={}'
            .format(args.modality, args.model, args.loss, args.nz, args.nf, args.batch_size, args.lr, args.gamma))
    train_csv = os.path.join(output_directory, 'train.csv')
    test_csv = os.path.join(output_directory, 'test.csv')
    best_txt = os.path.join(output_directory, 'best.txt')

    # optimizer = optim.Adam(model.parameters(), lr=args.lr)
    optimizer = optim.SGD(model.parameters(), args.lr,
                          momentum=args.momentum,
                          weight_decay=args.weight_decay)

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            start_epoch = checkpoint['epoch']
            best_result = checkpoint['best_result']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))
    else:
        start_epoch=1
        try:
            os.makedirs(output_directory)
            with open(train_csv, 'w') as csvfile:   
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
            with open(test_csv, 'w') as csvfile:   
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
        except OSError:
            pass

    traindir = os.path.join(os.path.join('data', 'nyudepthv2'), 'train')
    train_dataset = nyu_dataset.ImageFolder(traindir, type='train', modality=args.modality, image_size=image_size)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=args.batch_size,
                                             shuffle=True, num_workers=int(args.workers))

    valdir = os.path.join(os.path.join('data', 'nyudepthv2'), 'val')
    val_dataset = nyu_dataset.ImageFolder(valdir, type='val', modality=args.modality, image_size=image_size)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=8,
                                             shuffle=False, num_workers=int(args.workers))

    fixed_z = Variable(torch.randn(64, args.nz))
    if args.cuda:
        fixed_z = fixed_z.cuda()
    for epoch in range(start_epoch, args.epochs + 1):
        adjust_learning_rate(optimizer, epoch)
        train(train_loader, model, optimizer, epoch)
        result = test(val_loader, model, epoch)

        # generate images from random noise
        sample = model.decode(fixed_z)
        sample_rgb = sample[:, 0:3].cpu()
        sample_depth = sample[:, 3:4].cpu()
        save_image(sample_rgb.data, output_directory + '/sample_rgb_' + str(epoch) + '.png')
        save_image(sample_depth.data, output_directory + '/sample_depth_' + str(epoch) + '.png')

        # save best model
        is_best = result.rmse < best_result.rmse
        if is_best:
            best_result = result
            with open(best_txt, 'w') as txtfile:
                txtfile.write("epoch={}\nmse={:.3f}\nrmse={:.3f}\nabsrel={:.3f}\nlg10={:.3f}\nmae={:.3f}\ndelta1={:.3f}\nt_gpu={:.4f}\nkld={:.4f}\n".
                    format(epoch, result.mse, result.rmse, result.absrel, result.lg10, result.mae, result.delta1, result.gpu_time, result.kld))

        # save checkpoint
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_result': best_result,
            'optimizer' : optimizer.state_dict(),
        }, is_best, epoch)


def train(train_loader, model, optimizer, epoch):
    average_meter = AverageMeter()
    model.train()

    end = time.time()
    for batch_idx, (target, _) in enumerate(train_loader):
        target = Variable(target)
        if args.cuda:
            target = target.cuda()
        data_time = time.time() - end

        # compute output
        recon_batch, mu, logvar = model(target)
        loss = loss_function(recon_batch, target, mu, logvar)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure accuracy and record loss
        torch.cuda.synchronize()
        batch_time = time.time() - end
        result = Result()
        result.evaluate(recon_batch.data, target.data, mu.data, logvar.data)
        average_meter.update(result, batch_time, data_time, target.size(0))
        end = time.time()

        if batch_idx % args.log_interval == 0:
            print('=> output: {}'.format(output_directory))
            print('Train Epoch: {0} [{1}/{2}]\t'
                  't_Data={data_time:.3f}({average.data_time:.3f}) '
                  't_GPU={gpu_time:.3f}({average.gpu_time:.3f}) '
                  'RMSE={result.rmse:.3f}({average.rmse:.3f}) '
                  'MAE={result.mae:.3f}({average.mae:.3f}) '
                  'Delta1={result.delta1:.3f}({average.delta1:.3f}) '
                  'REL={result.absrel:.3f}({average.absrel:.3f}) '
                  'Lg10={result.lg10:.3f}({average.lg10:.3f}) '
                  'KLD={result.kld:.1f}({average.kld:.3f}) '.format(
                  epoch, batch_idx+1, len(train_loader), data_time=data_time, 
                  gpu_time=batch_time-data_time, result=result, average=average_meter.average()))

    avg = average_meter.average()
    with open(train_csv, 'a') as csvfile: 
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'mse': avg.mse, 'rmse': avg.rmse, 'absrel': avg.absrel, 'lg10': avg.lg10,
            'mae': avg.mae, 'delta1': avg.delta1, 'delta2': avg.delta2, 'delta3': avg.delta3, 
            'batch_time': avg.batch_time, 'data_time': avg.data_time, 'gpu_time': avg.gpu_time,
            'kld': avg.kld})


def test(val_loader, model, epoch):
    average_meter = AverageMeter()
    model.eval()
    
    end = time.time()
    for i, (target, _) in enumerate(val_loader):
        if args.cuda:
            target = target.cuda()
        target = Variable(target, volatile=True)
        data_time = time.time() - end

        # compute output
        recon_batch, mu, logvar = model(target)
        loss = loss_function(recon_batch, target, mu, logvar)
        
        # measure accuracy and record loss
        torch.cuda.synchronize()
        batch_time = time.time() - end
        result = Result()
        result.evaluate(recon_batch.data, target.data, mu.data, logvar.data)
        average_meter.update(result, batch_time, data_time, target.size(0))
        end = time.time()

        if i == 0:
            n = min(target.size(0), 8)
            comparison = torch.cat([target[:n, 0:3], recon_batch[:n, 0:3]])
            save_image(comparison.data.cpu(), output_directory + '/test_rgb_' + str(epoch) + '.png', nrow=n)
            if args.modality == 'rgbd':
                comparison = torch.cat([target[:n, 3:4], recon_batch[:n, 3:4]])
                save_image(comparison.data.cpu(), output_directory + '/test_depth_' + str(epoch) + '.png', nrow=n)

    avg = average_meter.average()

    with open(test_csv, 'a') as csvfile: 
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'mse': avg.mse, 'rmse': avg.rmse, 'absrel': avg.absrel, 'lg10': avg.lg10,
            'mae': avg.mae, 'delta1': avg.delta1, 'delta2': avg.delta2, 'delta3': avg.delta3, 
            'batch_time': avg.batch_time, 'data_time': avg.data_time, 'gpu_time': avg.gpu_time,
            'kld': avg.kld})

    print('\n*\n'
        'RMSE={average.rmse:.3f}\n'
        'MAE={average.mae:.3f}\n'
        'Delta1={average.delta1:.3f}\n'
        'REL={average.absrel:.3f}\n'
        'Lg10={average.lg10:.3f}\n'
        't_GPU={time:.3f}\n'
        'KLD={average.kld:.2f}\n'.format(
        average=avg, time=avg.gpu_time))

    return avg

def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 10 epochs"""
    lr = args.lr * (0.1 ** (epoch // 20))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def save_checkpoint(state, is_best, epoch):
    checkpoint_filename = os.path.join(output_directory, 'checkpoint-' + str(epoch) + '.pth.tar')
    torch.save(state, checkpoint_filename)
    if is_best:
        best_filename = os.path.join(output_directory, 'model_best.pth.tar')
        shutil.copyfile(checkpoint_filename, best_filename)
    if epoch > 1:
        prev_checkpoint_filename = os.path.join(output_directory, 'checkpoint-' + str(epoch-1) + '.pth.tar')
        if os.path.exists(prev_checkpoint_filename):
            os.remove(prev_checkpoint_filename)

if __name__ == '__main__':
    main()

