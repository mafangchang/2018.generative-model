import os
import torch
import torch.nn as nn
import torchvision.models
from torch.autograd import Variable
import collections
import math

def weights_init(m):
    # Initialize kernel weights with Gaussian distributions
    if isinstance(m, nn.Conv2d): 
        n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
        m.weight.data.normal_(0, math.sqrt(2. / n))
        if m.bias is not None: 
            m.bias.data.zero_()
    elif isinstance(m, nn.ConvTranspose2d):
        n = m.kernel_size[0] * m.kernel_size[1] * m.in_channels
        m.weight.data.normal_(0, math.sqrt(2. / n))
        if m.bias is not None: 
            m.bias.data.zero_()
    elif isinstance(m, nn.BatchNorm2d):
        m.weight.data.fill_(1)
        m.bias.data.zero_()

class Encoder(nn.Module):
    def __init__(self, modality):
        super(Encoder, self).__init__()
        pretrained_model = torchvision.models.__dict__['resnet18'](pretrained=True)

        if len(modality) == 3:
            self.conv1 = pretrained_model._modules['conv1']
            self.bn1 = pretrained_model._modules['bn1']
        else:
            self.conv1 = nn.Conv2d(len(modality), 64, kernel_size=7, stride=2, padding=3, bias=False)
            self.bn1 = nn.BatchNorm2d(64)
            weights_init(self.conv1)
            weights_init(self.bn1)
        
        self.relu1 = pretrained_model._modules['relu']
        self.maxpool = pretrained_model._modules['maxpool']
        self.layer1 = pretrained_model._modules['layer1']
        self.layer2 = pretrained_model._modules['layer2']
        self.layer3 = pretrained_model._modules['layer3']
        self.layer4 = pretrained_model._modules['layer4']
        self.conv2 =  nn.Sequential(
            nn.Conv2d(512, 32, kernel_size=1, bias=False),
            nn.BatchNorm2d(32),
            nn.ReLU(True)
            )
        self.bn2 = nn.BatchNorm2d(32)
        self.relu2 = nn.ReLU(True)

        self.conv2.apply(weights_init)
        self.bn2.apply(weights_init)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        # state size. 512 x 8 x 8

        x = self.conv2(x)
        x = self.bn2(x)
        x = self.relu2(x)
        # state size. 32 x 8 x 8

        return x


class Decoder(nn.Module):
    def convt(self, in_channels, out_channels, activation='relu'):
        bias = False
        kernel_size, stride, padding, output_padding = 3, 2, 1, 1
        if activation == 'relu':
            return nn.Sequential(
                nn.ConvTranspose2d(in_channels,out_channels,
                    kernel_size,stride,padding,output_padding),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(True),
            )
        elif activation == 'sigmoid':
            return nn.Sequential(
                nn.ConvTranspose2d(in_channels,out_channels,
                    kernel_size,stride,padding,output_padding),
                nn.BatchNorm2d(out_channels),
                nn.Sigmoid(),
            )

    def __init__(self, modality):
        super(Decoder, self).__init__()

        self.convt1 = self.convt(32, 32)
        self.convt2 = self.convt(32, 32)
        self.convt3 = self.convt(32, 16)
        self.convt4 = self.convt(16, 8)
        self.convt5 = self.convt(8, 4, activation='sigmoid')
        # self.upsample = nn.Upsample(scale_factor=2, mode='bilinear')

        self.convt1.apply(weights_init)
        self.convt2.apply(weights_init)
        self.convt3.apply(weights_init)
        self.convt4.apply(weights_init)
        self.convt5.apply(weights_init)

    def forward(self, z):

        # state size. 32 x 8 x 8
        z = self.convt1(z)
        # state size. 32 x 16 x 16
        z = self.convt2(z)
        # state size. 32 x 32 x 32
        z = self.convt3(z)
        # state size. 16 x 64 x 64
        z = self.convt4(z)
        # state size. 8 x 128 x 128
        z = self.convt5(z)
        # state size. 4 x 256 x 256

        return z
