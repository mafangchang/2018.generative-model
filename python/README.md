
## Dataset
TODO

## Variational Auto Encoder
This is an improved implementation of the paper [Stochastic Gradient VB and the
Variational Auto-Encoder](http://arxiv.org/abs/1312.6114) by Kingma and Welling.
It uses ReLUs and the adam optimizer, instead of sigmoids and adagrad. These changes make the network converge much faster.

### Usage
```
usage: vae_train.py
```

## Deep Convolution Generative Adversarial Networks

This example implements the paper [Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks](http://arxiv.org/abs/1511.06434)

After every 100 training iterations, the files `real_samples.png` and `fake_samples.png` are written to disk
with the samples from the generative model.

After every epoch, models are saved to: `netG_epoch_%d.pth` and `netD_epoch_%d.pth`

### Usage
```
usage: dcgan_train.py [-h] [--dataset DATASET] [--modality MODALITY]
                      [--dataroot DATAROOT] [--workers WORKERS]
                      [--batchSize BATCHSIZE] [--imageSize IMAGESIZE]
                      [--nz NZ] [--ngf NGF] [--ndf NDF] [--niter NITER]
                      [--lr LR] [--beta1 BETA1] [--cuda] [--ngpu NGPU]
                      [--netG NETG] [--netD NETD] [--manualSeed MANUALSEED]

optional arguments:
  -h, --help            show this help message and exit
  --dataset DATASET     nyudepthv2 | cifar10 | lsun | imagenet | folder | lfw
                        | fake
  --modality MODALITY   rgb | rgbd
  --dataroot DATAROOT   path to dataset
  --workers WORKERS     number of data loading workers
  --batchSize BATCHSIZE
                        input batch size
  --imageSize IMAGESIZE
                        the height / width of the input image to network
  --nz NZ               size of the latent z vector
  --ngf NGF
  --ndf NDF
  --niter NITER         number of epochs to train for
  --lr LR               learning rate, default=0.0002
  --beta1 BETA1         beta1 for adam. default=0.5
  --cuda                enables cuda
  --ngpu NGPU           number of GPUs to use
  --netG NETG           path to netG (to continue training)
  --netD NETD           path to netD (to continue training)
  --manualSeed MANUALSEED
                        manual seed
```