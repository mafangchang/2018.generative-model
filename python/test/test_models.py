import unittest
import os
import time
import torch
import torch.nn as nn
import dataloader as nyu_dataset
import vae_models

criterion = nn.MSELoss()

class TestModels(unittest.TestCase):
    # unit tests
        
    def test_resnet(self):
        modality = 'rgbd'
        in_channels = len(modality)
        valdir = os.path.join(os.path.join('data', 'nyudepthv2'), 'val')
        val_dataset = nyu_dataset.ImageFolder(valdir, type='val', modality=modality)
        input_tensor, depth_tensor = val_dataset.__getitem__(0)

        # add singleton dimensions to create a batch
        while input_tensor.dim() < 4:
            input_tensor = input_tensor.unsqueeze(0)
        while depth_tensor.dim() < 4:
            depth_tensor = depth_tensor.unsqueeze(0)
        input_tensor = input_tensor.cuda()
        depth_tensor = depth_tensor.cuda()
        input_var = torch.autograd.Variable(input_tensor, volatile=False)
        target_var = torch.autograd.Variable(depth_tensor, volatile=False)

        print('testing ResNet')
        model = vae_models.ResNet_VAE(modality='rgbd').cuda()

        # model.train()
        # optimizer = torch.optim.SGD(model.parameters(), 0.01)

        # # forward pass
        # start_time = time.time()
        mu, logvar = model.encode(input_var)
        # reconstruct, mu, logvar = model.forward(input_var)
        # print(reconstruct.size())
        print(mu.size())
        print(logvar.size())
        # loss = criterion(output, target_var)
        # print('\tforward pass time={}'.format(time.time()-start_time))

        # # compute gradient and do SGD step
        # start_time = time.time()
        # optimizer.zero_grad()
        # loss.backward()
        # optimizer.step()
        # print('\tbackward pass time={}'.format(time.time()-start_time))

                
if __name__ == '__main__':
    unittest.main()