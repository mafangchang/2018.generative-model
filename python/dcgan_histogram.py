import argparse
import os
import torch
import torch.nn as nn
import torch.nn.parallel
# import torch.backends.cudnn as cudnn
from dcgan_models import _netG

import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--model-path', default=os.path.join('dcgan_results', 'nyudepthv2.modality=rgb.nz=100.ngf=64.bs=32', 'netG_epoch_39.pth'), help='path to model')
parser.add_argument('-l', '--layer', default=0, type=int, help='which layer')

opt = parser.parse_args()
print(opt)
netG = _netG(0)
print(netG)
netG.load_state_dict(torch.load(opt.model_path))
weight = netG.main[opt.layer].weight.data.numpy()
print(weight.shape)

print("==> reshaping data")
data = weight[:,:,:,:].reshape(-1)

print("==> creating histogram")
plt.hist(data, bins=100)  

try:
    output_folder = 'dcgan_figures'
    os.makedirs(output_folder)
except OSError:
    pass
print("==> saving and showing plot")
filename = os.path.join(output_folder, 'hist.layer={}.png'.format(opt.layer))
plt.savefig(filename)
plt.show()