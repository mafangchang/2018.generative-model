import argparse
import os
import torch
import torch.nn as nn
import torch.nn.parallel
# import torch.backends.cudnn as cudnn
from vae_models import ResNet_VAE

import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--model-path', default=os.path.join('dcgan_results', 'nyudepthv2.modality=rgb.nz=100.ngf=64.bs=32', 'netG_epoch_39.pth'), help='path to model')
parser.add_argument('-l', '--layer', default=0, type=int, help='which layer')

opt = parser.parse_args()
print(opt)
vae = ResNet_VAE('rgbd', 400)
print(vae)
checkpoint = torch.load('vae_results/modality=rgbd.model=resnet.loss=l1.nz=400.nf=128.bs=8.lr=0.1.gamma=0.0/model_best.pth.tar')
vae.load_state_dict(checkpoint['state_dict'])
weight = vae.convt4[0].weight.data.numpy()
print(weight.shape)

print("==> reshaping data")
data = weight.reshape(-1)

print("==> creating histogram")
plt.hist(data, bins=100)  

try:
    output_folder = 'vae_figures/weights'
    os.makedirs(output_folder)
except OSError:
    pass
print("==> saving and showing plot")
filename = os.path.join(output_folder, 'hist.png'.format(opt.layer))
plt.savefig(filename)
plt.show()