import torch
from torch.autograd import Variable

import matplotlib.pyplot as plt
import numpy as np
import math

def create_sampling_matrix(sampling_strategy, percentage_samples, image_size, modality):
    nc = len(modality)
    if sampling_strategy == 'uniform':
        sampling_matrix = (np.random.uniform(0.0, 1.0, size=(1,image_size,image_size)) < percentage_samples).astype(float)
        sampling_matrix = np.repeat(sampling_matrix, nc, axis=0)
    else:
        percentage_samples = 0
        if 'r' in sampling_strategy and 'r' in modality:
            sampling_matrix_r = np.ones(shape=(1,image_size,image_size))
            percentage_samples += 1.0/nc
        else:
            sampling_matrix_r = np.zeros(shape=(1,image_size,image_size))

        if 'g' in sampling_strategy and 'g' in modality:
            sampling_matrix_g = np.ones(shape=(1,image_size,image_size))
            percentage_samples += 1.0/nc
        else:
            sampling_matrix_g = np.zeros(shape=(1,image_size,image_size))

        if 'b' in sampling_strategy and 'b' in modality:
            sampling_matrix_b = np.ones(shape=(1,image_size,image_size))
            percentage_samples += 1.0/nc
        else:
            sampling_matrix_b = np.zeros(shape=(1,image_size,image_size))

        if 'd' in sampling_strategy and 'd' in modality:
            sampling_matrix_d = np.ones(shape=(1,image_size,image_size))
            percentage_samples += 1.0/nc
        else:
            sampling_matrix_d = np.zeros(shape=(1,image_size,image_size))

        if modality == 'rgbd':
            sampling_matrix = np.concatenate((sampling_matrix_r,sampling_matrix_g,sampling_matrix_b, sampling_matrix_d), axis=0)
        elif modality == 'rgb':
            sampling_matrix = np.concatenate((sampling_matrix_r,sampling_matrix_g,sampling_matrix_b), axis=0)

    sampling_matrix_pth = Variable(torch.from_numpy(sampling_matrix).float(), volatile=False)
    sampling_matrix_pth = sampling_matrix_pth.cuda()

    # print(sampling_matrix.shape)
    return sampling_matrix_pth, sampling_matrix, percentage_samples


def evaluate(output, target):
    # nan_mask = ((target<=0) + (target.ne(target))) > 0
    # output[nan_mask] = 0
    # target[nan_mask] = 0
    # num_valid_pixels = float(target.nelement() - torch.sum(nan_mask))
    num_valid_pixels = torch.numel(output)

    diff = torch.abs(output - target)
    

    mse = torch.sum(torch.pow(diff, 2)) / num_valid_pixels
    rmse = math.sqrt(mse)
    mae = torch.sum(diff) / num_valid_pixels

    # lg10 = torch.abs(log10(output) - log10(target))
    # lg10[nan_mask] = 0
    # self.lg10 = torch.sum(lg10) / num_valid_pixels
    # del lg10

    rel = diff / target
    # rel[nan_mask] = 0
    absrel = torch.sum(rel) / num_valid_pixels
    # del rel

    # ratio1 = output / target
    # ratio2 = target / output
    # maxRatio = maxOfTwo(ratio1, ratio2)
    # self.delta1 = torch.sum(maxRatio < 1.25) / num_valid_pixels
    # self.delta2 = torch.sum(maxRatio < math.pow(1.25,2)) / num_valid_pixels
    # self.delta3 = torch.sum(maxRatio < math.pow(1.25,3)) / num_valid_pixels

    # compute the reconstruction error
    # loss_rgb = criterion(output[0, 0:3], y[0:3])
    # loss_rgb = loss_rgb.data.cpu().numpy()[0] / torch.sum(torch.pow(y[0:3], 2)).data.cpu().numpy()[0]

    # loss_depth = criterion(output[0, 3], y[3])
    # loss_depth = loss_depth.data.cpu().numpy()[0] / torch.sum(torch.pow(y[3], 2)).data.cpu().numpy()[0]

    # return loss_rgb, loss_depth
    return rmse, mae, absrel

def display(groundtruth, measurements, reconstruction, filename,
        percentage_samples, rgb_rmse, depth_mae, depth_rel):
    # visualize reconstruction and ground truth
    # Two subplots, the axes array is 1-d
    fig = plt.figure(figsize=(6, 8), dpi=200)
    ax1 = fig.add_subplot(3,2,1, adjustable='box', aspect=1)
    ax2 = fig.add_subplot(3,2,2, adjustable='box', aspect=1)
    ax3 = fig.add_subplot(3,2,3, adjustable='box', aspect=1)
    ax4 = fig.add_subplot(3,2,4, adjustable='box', aspect=1)
    ax5 = fig.add_subplot(3,2,5, adjustable='box', aspect=1)
    ax6 = fig.add_subplot(3,2,6, adjustable='box', aspect=1)
    
    # ground truth RGB
    rgb_gt = np.transpose(np.squeeze(groundtruth[0:3].data.cpu().numpy()), (1, 2, 0))
    ax1.imshow(rgb_gt, extent=[0,100,0,1], aspect=100)
    ax1.set_title('ground truth RGB')
    ax1.axis('off')

    # ground truth Depth
    depth_gt = np.squeeze(groundtruth[3:4].data.cpu().numpy())
    ax2.imshow(depth_gt, extent=[0,100,0,1], aspect=100)
    ax2.set_title('ground truth Depth')
    ax2.axis('off')

    # measurements
    rgb_measurements = np.transpose(np.squeeze(measurements[0:3].data.cpu().numpy()), (1, 2, 0))
    depth_measurements = np.squeeze(measurements[3:4].data.cpu().numpy())
    ax3.imshow(rgb_measurements, extent=[0,100,0,1], aspect=100)
    ax3.set_title('rgb measurements={:.1f}%'.format(100*percentage_samples))
    ax3.axis('off')
    ax4.imshow(depth_measurements, extent=[0,100,0,1], aspect=100)
    ax4.set_title('depth measurements={:.1f}%'.format(100*percentage_samples))
    ax4.axis('off')

    # reconstructed RGB
    img_rec = np.transpose(np.squeeze(reconstruction.cpu().numpy()), (1, 2, 0))

    rgb_rec = img_rec[:,:,0:3]
    ax5.imshow(rgb_rec, extent=[0,100,0,1], aspect=100)
    ax5.set_title( 'rmse={:.3f}'.format(rgb_rmse) )
    ax5.axis('off')


    # reconstructed Depth
    depth_rec = img_rec[:,:,3]
    ax6.imshow(depth_rec, extent=[0,100,0,1], aspect=100)
    ax6.set_title( 'mae={:.1f}cm, rel={:.1f}%'.format(100*depth_mae, 100*depth_rel) )
    ax6.axis('off')

    fig.savefig(filename, format='png', bbox_inches='tight')

    plt.show()