from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable

import numpy as np
import time
import math


import dataloader as nyu_dset
import utils

parser = argparse.ArgumentParser(description='Invertible Neural Networks')
parser.add_argument('-a', '--arch', default='autoencoder', help='autoencoder | vae-simple | vae-resnet | dcgan')
parser.add_argument('--dataset', default='nyudepthv2', help='nyudepthv2 | fake')
parser.add_argument('--modality', default='rgbd', help='rgb | rgbd')
parser.add_argument('-s', '--sampling-strategy', default='uniform', help="options: uniform, or any combination of 'r', 'g' and 'b'")
parser.add_argument('-p', '--percentage-samples', type=float, default=1, help='percentage of samples')
args = parser.parse_args()

if not torch.cuda.is_available():
    print("Error: CUDA not available")

nc = len(args.modality)

criterion = nn.MSELoss(size_average=False).cuda()
# criterion = nn.L1Loss(size_average=True).cuda()


def main():
    global iter

    try:
        output_folder = "{}_figures".format(args.arch)
        os.makedirs(output_folder)
    except OSError:
        pass

    # load saved model
    if args.arch == 'autoencoder':
        from autoencoder_models import Encoder, Decoder
        checkpoint = torch.load('autoencoder_results/modality=rgbd.bs=8.lr=0.1/model_best.pth.tar')

        # encoder = Encoder(args.modality)
        # encoder.load_state_dict(checkpoint['encoder_state_dict'])
        # encoder.cuda()
        # encoder.eval()

        decoder = Decoder(args.modality)
        decoder.load_state_dict(checkpoint['decoder_state_dict'])
        decoder.cuda()
        decoder.eval()

        image_size = 256
        z = Variable(torch.zeros(1, 32, 8, 8).cuda(), requires_grad=True)
    elif 'vae' in args.arch:
        nz = 400
        if args.arch == 'vae-simple':
            from vae_models import NYU_VAE
            image_size = 128
            nf = 64
            vae = NYU_VAE(nc, nf, nz)
            checkpoint = torch.load('vae_results/modality=rgbd.model=simple.loss=l1.nz=400.nf=64.bs=8.lr=0.001/model_best.pth.tar')
        elif args.arch == 'vae-resnet':
            from vae_models import ResNet_VAE  
            image_size = 256
            nf = 128      
            vae = ResNet_VAE(args.modality, nz)
            checkpoint = torch.load('vae_results/modality=rgbd.model=resnet.loss=l1.nz=400.nf=128.bs=8.lr=0.1.gamma=0.0/model_best.pth.tar')
    
        vae.load_state_dict(checkpoint['state_dict'])
        vae.cuda()
        vae.eval()
        decoder = vae.decode

        z = Variable(torch.zeros(1, nz).cuda(), requires_grad=True)
    elif args.arch == 'dcgan':
        assert args.modality=='rgb', "no trained model available for dcgan, unless for RGB"
        from dcgan_models import _netG
        checkpoint = torch.load('dcgan_results/nyudepthv2.modality=rgb.nz=100.ngf=64.bs=64/netG_epoch_55.pth')
        decoder = _netG(1).cuda()
        decoder.load_state_dict(checkpoint)

        image_size = 128
        nz = 100
        ngf = 64

        z = Variable(torch.zeros(1, nz).cuda(), requires_grad=True)
    else:
        assert False, "invalid option"

    # print(decoder)

    sampling_matrix_pth, sampling_matrix, percentage_samples = \
        utils.create_sampling_matrix(args.sampling_strategy, args.percentage_samples, image_size, args.modality)

    # create data loader
    if args.dataset == 'nyudepthv2':
        # create dataloader for real image test
        valdir = os.path.join('data', 'nyudepthv2', 'val')
        dataset = nyu_dset.ImageFolder(valdir, type='val', modality=args.modality, image_size=image_size)
        input_tensor, depth_tensor = dataset.__getitem__(0)

        input_tensor = input_tensor.cuda()
        depth_tensor = depth_tensor.cuda()

        y_gt = Variable(input_tensor, volatile=False)
        y = sampling_matrix_pth * y_gt
    elif args.dataset == 'fake':
        # create random ground truth signal of interest
        if args.arch == 'autoencoder':
            x_gt_pth = torch.randn(1, 32, 8, 8).cuda()
        elif 'vae' in args.arch:
            x_gt_pth = torch.randn(1, nz).cuda()
        elif args.arch == 'dcgan':
            x_gt_pth = torch.randn(1, nz, 1, 1).cuda()
        else:
            pass
        y_gt = torch.squeeze(decoder(Variable(x_gt_pth)))
        y = sampling_matrix_pth * y_gt
        y = torch.squeeze(y)
    
    y = y.detach()

    start_time = time.time()

    # optimization with gradient
    print('running solver for {}..'.format(args.arch))
    print('percsample = {:.1f}'.format(100 * float(np.sum(sampling_matrix))/sampling_matrix.size))
    iter = 0
    lr = 0.05
    optimizer = optim.Adam([z], lr=lr)
    for iter in range(2000):
        # if iter==1000:
        #     for param_group in optimizer.param_groups:
        #         param_group['lr'] = lr/10
        optimizer.zero_grad()
        x = decoder(z)
        loss = criterion(sampling_matrix_pth * x, y)
        loss.backward()
        optimizer.step()

        if iter % 100 == 0:
            print('==> iter={}, loss={}'.format( iter, loss.data[0]))
    
    output = x.data
    target = y_gt.data.float()
    rgb_rmse, rgb_mae, rgb_rel = utils.evaluate(output[0,0:3], target[0:3])
    depth_rmse, depth_mae, depth_rel = utils.evaluate(output[0,3], target[3])
    depth_rmse, depth_mae = 10*depth_rmse, 10*depth_mae
    overall_rmse, overall_mae, overall_rel = utils.evaluate(output, target)
    
    print('\nresult:')
    print('==> overall: rmse={:.3f}, mae={:.3f}'.format(overall_rmse, overall_mae))
    print('==> rgb: rmse={:.3f}, mae={:.3f}'.format(rgb_rmse, rgb_mae))
    print('==> depth: rmse={:.3f}, mae={:.3f}, rel={:.1f}%'.format(depth_rmse, depth_mae, 100*depth_rel))
    print('==> reconstruction time={:.2f}s'.format(time.time()-start_time))

    # save results
    filename = 'recon.dataset={}.modality={}.sampling={}.percsample={}.png'.format(
        args.dataset, args.modality, args.sampling_strategy, args.percentage_samples)
    filename = os.path.join(output_folder, filename)

    utils.display(y_gt, y, output, filename, 
        args.percentage_samples, rgb_rmse, depth_mae, depth_rel)

if __name__ == '__main__':
    main()

