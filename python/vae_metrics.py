import torch
import math
import numpy as np

def log10(x):
    """Convert a new tensor with the base-10 logarithm of the elements of x. """
    return torch.log(x) / math.log(10)

def maxOfTwo(x, y):
    """Convert a new tensor with the bigger elements of x, y in each pixel. """
    z = y.clone()
    mask_x_large = x > y
    z[mask_x_large] = x[mask_x_large]
    return z

class Result(object):
    def __init__(self):
        self.mse, self.rmse, self.absrel, self.lg10, self.mae, \
        self.delta1, self.delta2, self.delta3, \
        self.batch_time, self.data_time, self.gpu_time,\
        self.kld = \
        0,0,0,0,0,0,0,0,0,0,0,0

    def set_to_worst(self):
        self.mse, self.rmse, self.absrel, self.lg10, self.mae, \
        self.delta1, self.delta2, self.delta3, \
        self.batch_time, self.data_time, self.gpu_time, self.kld = \
        np.inf,np.inf,np.inf,np.inf,np.inf,0,0,0,0,0,0,np.inf

    def update(self, mse, rmse, absrel, lg10, mae, delta1, delta2, delta3, batch_time, data_time, kld):
        self.mse, self.rmse, self.absrel, self.lg10, self.mae, \
        self.delta1, self.delta2, self.delta3, self.batch_time, self.data_time, self.kld = \
        mse, rmse, absrel, lg10, mae, delta1, delta2, delta3, batch_time, data_time, kld
        self.gpu_time = batch_time - data_time

    def evaluate(self, output, target, mu, logvar):
        nan_mask = ((target<=0) + (target.ne(target))) > 0
        output[nan_mask] = 0
        target[nan_mask] = 0
        num_valid_pixels = float(target.nelement() - torch.sum(nan_mask))

        diff = torch.abs(output - target)

        self.mse = torch.sum(torch.pow(diff, 2)) / num_valid_pixels
        self.rmse = math.sqrt(self.mse)
        self.mae = torch.sum(diff) / num_valid_pixels

        lg10 = torch.abs(log10(output) - log10(target))
        lg10[nan_mask] = 0
        self.lg10 = torch.sum(lg10) / num_valid_pixels
        del lg10

        rel = diff / target
        rel[nan_mask] = 0
        self.absrel = torch.sum(rel) / num_valid_pixels
        del rel

        ratio1 = output / target
        ratio2 = target / output
        maxRatio = maxOfTwo(ratio1, ratio2)
        self.delta1 = torch.sum(maxRatio < 1.25) / num_valid_pixels
        self.delta2 = torch.sum(maxRatio < math.pow(1.25,2)) / num_valid_pixels
        self.delta3 = torch.sum(maxRatio < math.pow(1.25,3)) / num_valid_pixels

        self.batch_time = 0
        self.data_time = 0
        self.gpu_time = 0

        self.kld = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()) 

    def __print__(self):
        print(  "mse={} rmse={} rel={} lg10={}\n"
                "mae={}, delta1={}, delta2={}, delta3={}\n"
                "batch_time={}, data_time={}, gpu_time={}, kld={}".
                format(self.mse, self.rmse, self.absrel, self.lg10,
                     self.mae, self.delta1, self.delta2, self.delta3,
                     self.batch_time, self.data_time, self.gpu_time, self.kld))


class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.count = 0.0

        self.sum_mse = 0
        self.sum_rmse = 0
        self.sum_absrel = 0
        self.sum_lg10 = 0
        self.sum_mae = 0
        self.sum_delta1 = 0
        self.sum_delta2 = 0
        self.sum_delta3 = 0
        self.sum_batch_time = 0
        self.sum_data_time = 0
        self.sum_gpu_time = 0
        self.sum_kld = 0

    def update(self, result, batch_time, data_time, n=1):
        self.count += n

        self.sum_mse += n*result.mse
        self.sum_rmse += n*result.rmse
        self.sum_absrel += n*result.absrel
        self.sum_lg10 += n*result.lg10
        self.sum_mae += n*result.mae
        self.sum_delta1 += n*result.delta1
        self.sum_delta2 += n*result.delta2
        self.sum_delta3 += n*result.delta3
        self.sum_batch_time += n*batch_time
        self.sum_data_time += n*data_time
        self.sum_gpu_time = self.sum_batch_time - self.sum_data_time
        self.sum_kld += n*result.kld

    def average(self):
        avg = Result()
        avg.update(self.sum_mse / self.count, self.sum_rmse / self.count,
                   self.sum_absrel / self.count, self.sum_lg10 / self.count,
                   self.sum_mae / self.count, self.sum_delta1 / self.count,
                   self.sum_delta2 / self.count, self.sum_delta3 / self.count,
                   self.sum_batch_time / self.count, self.sum_data_time / self.count,
                   self.sum_kld / self.count)
        return avg