import os
import torch
import torch.nn as nn
import torchvision.models
from torch.autograd import Variable
import collections
import math

def weights_init(m):
    # Initialize kernel weights with Gaussian distributions
    if isinstance(m, nn.Conv2d): 
        n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
        m.weight.data.normal_(0, math.sqrt(2. / n))
        if m.bias is not None: 
            m.bias.data.zero_()
    elif isinstance(m, nn.ConvTranspose2d):
        n = m.kernel_size[0] * m.kernel_size[1] * m.in_channels
        m.weight.data.normal_(0, math.sqrt(2. / n))
        if m.bias is not None: 
            m.bias.data.zero_()
    elif isinstance(m, nn.BatchNorm2d):
        m.weight.data.fill_(1)
        m.bias.data.zero_()

class VAE(nn.Module):
    def __init__(self):
        super(VAE, self).__init__()

    def encode(self, x):
        pass

    def decode(self, z):
        pass

    def reparameterize(self, mu, logvar):
        if self.training:
          std = logvar.mul(0.5).exp_()
          eps = Variable(std.data.new(std.size()).normal_())
          return eps.mul(std).add_(mu)
        else:
          return mu

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

class ResNet_VAE(VAE):
    def convt(self, in_channels, out_channels, activation='relu'):
        bias = False
        kernel_size, stride, padding, output_padding = 3, 2, 1, 1
        if activation == 'relu':
            return nn.Sequential(
                nn.ConvTranspose2d(in_channels,out_channels,
                    kernel_size,stride,padding,output_padding),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(True),
            )
        elif activation == 'sigmoid':
            return nn.Sequential(
                nn.ConvTranspose2d(in_channels,out_channels,
                    kernel_size,stride,padding,output_padding),
                nn.BatchNorm2d(out_channels),
                nn.Sigmoid(),
            )

    def __init__(self, modality, nz):
        super(ResNet_VAE, self).__init__()
        pretrained_model = torchvision.models.__dict__['resnet18'](pretrained=True)

        if len(modality) == 3:
            self.conv1 = pretrained_model._modules['conv1']
            self.bn1 = pretrained_model._modules['bn1']
        else:
            self.conv1 = nn.Conv2d(len(modality), 64, kernel_size=7, stride=2, padding=3, bias=False)
            self.bn1 = nn.BatchNorm2d(64)
            weights_init(self.conv1)
            weights_init(self.bn1)
        
        self.relu1 = pretrained_model._modules['relu']
        self.maxpool = pretrained_model._modules['maxpool']
        self.layer1 = pretrained_model._modules['layer1']
        self.layer2 = pretrained_model._modules['layer2']
        self.layer3 = pretrained_model._modules['layer3']
        self.layer4 = pretrained_model._modules['layer4']
        self.conv2 =  nn.Sequential(
            nn.Conv2d(512, 32, kernel_size=1, bias=False),
            nn.BatchNorm2d(32),
            nn.ReLU(True)
            )
        # self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc1 = nn.Linear(2048, nz)
        self.fc2 = nn.Linear(2048, nz)
        self.fc3 = nn.Linear(nz, 2048)

        self.bn3 = nn.BatchNorm2d(2048)
        self.relu3 = nn.ReLU(True)

        self.convt1 = self.convt(128, 128)
        self.convt2 = self.convt(128, 64)
        self.convt3 = self.convt(64, 64)
        self.convt4 = self.convt(64, 32)
        self.convt5 = self.convt(32, 4, activation='sigmoid')
        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear')

        self.conv2.apply(weights_init)
        self.bn3.apply(weights_init)
        self.convt1.apply(weights_init)
        self.convt2.apply(weights_init)
        self.convt3.apply(weights_init)
        self.convt4.apply(weights_init)
        self.convt5.apply(weights_init)

    def encode(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        # state size. 512 x 8 x 8

        ## x = self.avgpool(x)
        ## state size. 512 x 2 x 2


        x = self.conv2(x)
        # state size. 32 x 8 x 8
        
        x = x.view(x.size(0), -1)
        # # state size. 2048

        mu = self.fc1(x)
        logvar = self.fc2(x)
        return mu, logvar 

    def decode(self, z):
        # state size. 400
        z = self.fc3(z)
        z = self.bn3(z)
        z = self.relu3(z)
        # state size. 2048
        z = z.view(z.size(0), 128, 4, 4)
        # state size. 128 x 4 x 4
        z = self.convt1(z)
        # state size. 128 x 8 x 8
        z = self.convt2(z)
        # state size. 64 x 16 x 16
        z = self.convt3(z)
        # state size. 64 x 32 x 32
        z = self.convt4(z)
        # state size. 32 x 64 x 64
        z = self.convt5(z)
        # state size. 4 x 128 x 128
        z = self.upsample(z)
        # state size. 4 x 256 x 256

        return z
        # h3 = z.view(-1, 8*self.nf, 4, 4)
        # return self.seq2(h3)

class NYU_VAE(VAE):
    def __init__(self, nc, nf, nz):
        super(NYU_VAE, self).__init__()

        self.nc = nc
        self.nf = nf
        self.nz = nz

        # encoder
        self.seq1 = nn.Sequential(
            # input is (nc) x 128 x 128
            nn.Conv2d(nc, nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(nf),
            nn.LeakyReLU(0.2, True),
            # state size. (nf) x 64 x 64
            nn.Conv2d(nf, 2*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(2*nf),
            nn.LeakyReLU(0.2, True),
            # state size. (2*nf) x 32 x 32
            nn.Conv2d(2*nf, 4*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(4*nf),
            nn.LeakyReLU(0.2, True),
            # state size. (4*nf) x 16 x 16
            nn.Conv2d(4*nf, 8*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(8*nf),
            nn.LeakyReLU(0.2, True),
            # state size. (8*nf) x 8 x 8
            nn.Conv2d(8*nf, 8*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(8*nf),
            nn.LeakyReLU(0.2, True),
            # output size. (8*nf) x 4 x 4
        )

        # decoder
        self.seq2 = nn.Sequential(
            # input is (8*nf) x 4 x 4
            nn.ConvTranspose2d(8*nf, 8*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(8*nf),
            nn.ReLU(True),
            # state size. (8*nf) x 8 x 8
            nn.ConvTranspose2d(8*nf, 4*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(4*nf),
            nn.ReLU(True),
            # state size. (4*nf) x 16 x 16
            nn.ConvTranspose2d(4*nf, 2*nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(2*nf),
            nn.ReLU(True),
            # state size. (2*nf) x 32 x 32
            nn.ConvTranspose2d(2*nf, nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(nf),
            nn.ReLU(True),
            # state size. nf x 64 x 64
            nn.ConvTranspose2d(nf, nf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(nf),
            nn.ReLU(True),
            # state size. nf x 128 x 128
            nn.Conv2d(nf, nc, 1, 1, bias=False),
            nn.BatchNorm2d(nc),
            nn.Sigmoid()
            # output size. (nc) x 128 x 128
        )

        self.fc21 = nn.Linear((8*nf)*4*4, nz)
        self.fc22 = nn.Linear((8*nf)*4*4, nz)
        self.fc3 = nn.Linear(nz, (8*nf)*4*4)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

        # weights random init
        self.seq1.apply(weights_init)
        self.seq2.apply(weights_init)
        self.fc21.apply(weights_init)
        self.fc22.apply(weights_init)
        self.fc3.apply(weights_init)

    def encode(self, x):
        h1 = self.seq1(x).view(-1, (8*self.nf)*4*4)
        mu = self.fc21(h1)
        logvar = self.fc22(h1)
        return mu, logvar

    def decode(self, z):
        z = z.view(z.size(0),-1)
        h3 = self.relu(self.fc3(z)).view(-1, 8*self.nf, 4, 4)
        return self.seq2(h3)


# class Encoder(nn.Module):
#     def __init__(self):
#         super(Encoder, self).__init__()

#         self.main = nn.Sequential(
#             # input is (nc) x 128 x 128
#             nn.Conv2d(nc, nf, 4, 2, 1, bias=False),
#             nn.BatchNorm2d(nf),
#             nn.ReLU(True),
#             # state size. (nf) x 64 x 64
#             nn.Conv2d(nf, 2*nf, 4, 2, 1, bias=False),
#             nn.BatchNorm2d(2*nf),
#             nn.ReLU(True),
#             # state size. (2*nf) x 32 x 32
#             nn.Conv2d(2*nf, 4*nf, 4, 2, 1, bias=False),
#             nn.BatchNorm2d(4*nf),
#             nn.ReLU(True),
#             # state size. (4*nf) x 16 x 16
#             nn.Conv2d(4*nf, 1, 1, bias=False),
#             nn.BatchNorm2d(1),
#             nn.ReLU(True),
#             # output size. 1 x 16 x 16
#         )

#         self.fc21 = nn.Linear(256, 64)
#         self.fc22 = nn.Linear(256, 64)

#     def forward(self, x):
#         hidden = self.main(x).view(-1, 1, 256)
#         mu = self.fc21(hidden)
#         logvar = self.fc22(hidden)
#         return mu, logvar

# class Decoder(nn.Module):
#     def __init__(self):
#         super(Decoder, self).__init__()

#         self.main = nn.Sequential(
#             # input is 1 x 16 x 16
#             nn.ConvTranspose2d(1, nf, 4, 2, 1, bias=False),
#             nn.BatchNorm2d(nf),
#             nn.ReLU(True),
#             # state size. (nf) x 32 x 32
#             nn.ConvTranspose2d(nf, 2*nf, 4, 2, 1, bias=False),
#             nn.BatchNorm2d(2*nf),
#             nn.ReLU(True),
#             # state size. (2*nf) x 64 x 64
#             nn.ConvTranspose2d(2*nf, nc, 4, 2, 1, bias=False),
#             nn.Sigmoid()
#             # output size. (nc) x 128 x 128
#         )

#         self.fc3 = nn.Linear(64, 256)

#         self.relu = nn.ReLU()

#     def forward(self, x):
#         hidden = self.relu(self.fc3(x)).view(-1, 1, 16, 16)
#         return self.main(hidden)

class MNIST_VAE(nn.Module):
    def __init__(self):
        super(MNIST_VAE, self).__init__()

        self.fc1 = nn.Linear(784, 400)
        self.fc21 = nn.Linear(400, 20)
        self.fc22 = nn.Linear(400, 20)
        self.fc3 = nn.Linear(20, 400)
        self.fc4 = nn.Linear(400, 784)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def encode(self, x):
        h1 = self.relu(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
          std = logvar.mul(0.5).exp_()
          eps = Variable(std.data.new(std.size()).normal_())
          return eps.mul(std).add_(mu)
        else:
          return mu

    def decode(self, z):
        h3 = self.relu(self.fc3(z))
        return self.sigmoid(self.fc4(h3))

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 784))
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

